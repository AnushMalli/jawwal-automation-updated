*** Settings ***
Library           ExtendedSelenium2Library
Resource          ../../Resources/MainFunctions.robot
Resource          ../../Facilities/BaseCustomerFacilities.robot
Resource          NavigatorVariables.txt

*** Keywords ***
Go to CRM
    Open Browser    ${CRM_URL}    ie
    Log To Console    Browser opened
    #Maximize Browser Window
    Maximize Browser Window
    ${title}    Get Title
    Log To Console    ${title}
    #Select IFrame    id=navTabGroupDiv
    #Wait And Click Element    id=buttonClose
    Comment    Unselect Frame

GoToCustomerSearch
    Wait And Click Element    ${MainLink}
    Wait And Click Element    ${ServiceMenuTabButton}
    Wait And Click Element    ${ServiceToCustomerSearch}

GoToCorporateCustomersList
    Wait And Click Element    ${MainLink}
    Wait And Click Element    ${ServiceMenuTabButton}
    Wait And Click Element    ${ServiceToCorporate}

GoToIndividualCustomersList
    Wait And Click Element    ${MainLink}
    Wait And Click Element    ${ServiceMenuTabButton}
    Wait And Click Element    ${ServiceToIndividual}

GoToCases
    Wait And Click Element    ${MainLink}
    Wait And Click Element    ${ServiceMenuTabButton}
    Wait And Click Element    ${ServiceToCases}

GoToSubscriptions
    Wait And Click Element    ${MainLink}
    Wait And Click Element    ${SalesMenuTabButton}
    Wait And Click Element    ${SalesToSubscriptions}

OpenIndividualCustomerViewPage
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=formSelectorDropdown
    Wait And Click Element    ${OpenIndividualCustomer}
    Unselect Frame

OpenCorporateCustomerViewPage
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=formSelectorDropdown
    Wait And Click Element    ${OpenCorporateCustomerView}
    Unselect Frame

OpenCustomerDataPage
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=formSelectorDropdown
    Wait And Click Element    ${OpenCustomerData}
    Unselect Frame

OpenAccountSummaryPage
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=header_crmFormSelector
    Wait And Click Element    ${OpenAccountSummary}
    Unselect Frame

OpenMonetaryTransactionsPage
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=header_crmFormSelector
    Wait And Click Element    ${OpenMonetaryTransactions}
    Unselect Frame

OpenBillingForCorporatePage
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=formSelectorDropdown
    Wait And Click Element    ${OpenBillingForCorporate}
    Unselect Frame

OpenCustomer360Page
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=header_crmFormSelector
    #Wait And Click Element    ${OpenCustomer360}
    Page Should Contain    Automation
    Capture Page Screenshot
    Unselect Frame
    Close Browser

OpenBillingAndPaymentPage
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=formSelectorDropdown
    Wait And Click Element    ${OpenBillingAndPayment}
    Unselect Frame

OpenCustomer360BlacklistPage
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=header_crmFormSelector
    #Wait And Click Element    ${OpenCustomer360BlackList}
    Unselect Frame

OpenAccountHistory
    [Arguments]    ${type}
    Comment    Run keyword if    '${type}'=='corporate'    Wait And Click Element    ${OpenAccountHistoryButtonCorp}
    Comment    Run keyword if    '${type}'=='individual'    Wait And Click Element    ${OpenAccountHistoryButtonIndv}
    Wait And Click Element    id=etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button
    Wait And Click Element    id=etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button
    Comment    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=etel_subscription|NoRelationship|Form|jwl.etel_subscription.AccountHistory.Button

OpenAccHistoryFromSubsPage
    ClickMoreCommand
    Wait And Click Element    ${OpenAccountHistoryButtonSubs}

ClickMoreCommand
    Wait And Click Element    id=moreCommands

OpenAccountHistoryCorp
    Wait And Click Element    id=account|NoRelationship|Form|jwl.account.Actions.Button
    Wait And Click Element    id=account|NoRelationship|Form|jwl.account.BI.Button
    Comment    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=account|NoRelationship|Form|jwl.account.AccountHistory.Button

OpenAccountHistoryIndv
    Wait And Click Element    id=contact|NoRelationship|Form|jwl.contact.Actions.Button
    Wait And Click Element    id=contact|NoRelationship|Form|jwl.contact.BI.Button
    Comment    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=contact|NoRelationship|Form|jwl.contact.AccountHistory.Button

mine_open360page
    Select IFrame    id=contentIFrame1
    Sleep    40s
    Wait And Click Element    Xpath=.//*[@id='header_crmFormSelector']/nobr/span
    Wait And Click Element    ${OpenCustomer360}
    Wait Until Angular Ready
    Page Should Contain Element    id=subscriptions_header_h2
    Capture Page Screenshot
    Unselect Frame

GoToReverseWriteOff
    #wait for element    id=contentIFrame0
    #Select IFrame    id=contentIFrame0
    #Sleep    20s
    Wait And Click Element    Xpath=.//*[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']/span/a/span
    Wait And Click Element    Xpath=.//*[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']/span/a/span
    Sleep    10s
    Wait And Click Element    Xpath=.//*[@id='contact|NoRelationship|Form|jwl.contact.Button6.Button']/span/a
    Wait And Click Element    Xpath=.//*[@id='contact|NoRelationship|Form|jwl.contact.Button6.Buttonx']/span/a
    Unselect Frame

OpenIndCustomerWithCustID
    [Arguments]    ${CUSTOMER_ID}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    sleep    5s
    wait for element    id=crmGrid_findCriteria
    click on element    id=crmGrid_findCriteria
    send text to element    id=crmGrid_findCriteria    ${CUSTOMER_ID}
    Wait And Click Element    Xpath=.//*[@id='crmGrid_findCriteriaImg']
    Wait And Click Element    xpath=//a[starts-with(@id,'gridBodyTable_primaryField')]
    Unselect Frame

OpenSubWithContractID
    [Arguments]    ${Subscription_Id}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Comment    sleep    20s
    Run Keyword And Ignore Error    wait and click element    //h2[@id='tab_16_header_h2']
    Run Keyword And Ignore Error    wait and click element    id=tab_13_header_h2
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    Select IFrame    id=contentIFrame1
    wait and click element    //h2[@id='subscriptions_header_h2']
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    #### Get count of the webtable ###
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to evaluate the correct subscription name ###
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${External_id}=    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]
    \    log    ${External_id}
    \    ${T1}    Convert To String    ${External_Id}
    \    ${T2}    Convert To String    ${Subscription_Id}
    \    ${a}    Split String    ${T1}    CONTR
    \    ${b}    Split String    ${T2}    CONTR
    \    ${c}    Convert To String    ${b[1]}
    \    log    ${c}
    \    ${d}    Convert To String    ${a[1]}
    \    log    ${d}
    \    Run Keyword If    '${c}'=='${d}'    Double Click Element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]
    \    Exit For Loop If    '${c}'=='${d}'
    ##### For loop ends ####
    Unselect Frame

OpenTempDeactivationForSub
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    wait for element    id=contact|NoRelationship|Form|jwl.contact.OrderCapture.Button
    click on element    id=contact|NoRelationship|Form|jwl.contact.OrderCapture.Button
    Sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contact|NoRelationship|Form|jwl.contact.Button62.Button
    click on element    id=contact|NoRelationship|Form|jwl.contact.Button62.Button
    sleep    5s
    Wait Until Keyword Succeeds    20s    2s    Element Should Not Be Visible    //img[@id='loading']
    Run Keyword And Ignore Error    wait for element    id=contact|NoRelationship|Form|jwl.contact.Button64.Button
    Run Keyword And Ignore Error    click on element    id=contact|NoRelationship|Form|jwl.contact.Button64.Button
    Run Keyword And Ignore Error    wait for element    id=contact|NoRelationship|Form|jwl.contact.Button64.Button
    Run Keyword And Ignore Error    click on element    id=contact|NoRelationship|Form|jwl.contact.Button64.Button
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    #wait for element    id=contentIFrame1
    #Select Frame    id=contentIFrame1
    #Unselect Frame
    #Sleep    20s
    #wait for element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    #Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    #click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    #wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    #click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    #Wait for element until disappear    id=loading    20s    2s
    #wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.TemporaryDeactivation.Button']
    #sleep    5s
    #click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.TemporaryDeactivation.Button']
    #Wait for element until disappear    id=loading

TempDeactivationPageForRequestByCustomer
    [Arguments]    ${Deactivation_Reason}    ${Alternate_phone}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait and click element    xpath=//div[@id='jwl_deactivationreasonid']/div
    Wait And Click Element    //ul[@id='jwl_deactivationreasonid_i_IMenu']//li[@id='item0']
    Wait And Click Element    xpath=//div[@id='jwl_alternativephone']/div
    send text to element    id=jwl_alternativephone_i    ${Alternate_phone}
    Unselect Frame

SelectSubWithContractID
    [Arguments]    ${Subscription_Id}
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    sleep    10s
    Run Keyword And Ignore Error    wait and click element    //h2[@id='tab_24_header_h2']
    Unselect Frame
    Select IFrame    id=contentIFrame1
    wait and click element    //h2[@id='subscriptions_header_h2']
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    #### Get count of the webtable ###
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to evaluate the correct movie name ###
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${External_id}=    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]
    \    log    ${External_id}
    \    ${T1}    Convert To String    ${External_id}
    \    ${T2}    Convert To String    ${Subscription_Id}
    \    ${a}    Split String    ${T1}    CONTR
    \    ${b}    Split String    ${T2}    CONTR
    \    ${c}    Convert To String    ${b[1]}
    \    log    ${c}
    \    ${d}    Convert To String    ${a[1]}
    \    log    ${d}
    \    Run Keyword If    '${c}'=='${d}'    Click Element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]
    \    Exit For Loop If    '${c}'=='${d}'
    Wait for element until disappear    //img[@id='loading']    10s    2s
    ##### For loop ends ####
    Unselect Frame

OpenUpdatedAccountForSub
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=crmRibbonManager
    wait for element    id=commandContainer2
    wait for element    id=contact|NoRelationship|Form|jwl.contact.Actions.Button
    click on element    id=contact|NoRelationship|Form|jwl.contact.Actions.Button
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    sleep    10s
    Comment    Wait for element until disappear    //img[@id='loading']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contact|NoRelationship|Form|jwl.contact.UpdateAccount.Button
    click on element    id=contact|NoRelationship|Form|jwl.contact.UpdateAccount.Button

UpdateAccForSubPage
    [Arguments]    ${BillingClass}    ${UPFirst_Name}    ${UPSec_Name}    ${UPThr_Name}    ${UPLast_Name}    ${UPFull_Name}
    ...    ${UPAuthSig_Name}    ${UP_Gender}    ${UP_Email}    ${UP_PhoneNum}    ${UP_FaxNum}
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    ####Subscription profileData ####
    wait and click element    id=jwl_billingclassification
    Wait And Click Element    xpath=//select[@id='jwl_billingclassification_i']/option[contains(text(),'${BillingClass}')]
    Wait And Click Element    id=jwl_billingprofileaflg
    Wait And Click Element    id=jwl_billingprofilecflg
    Wait And Click Element    id=jwl_billingprofilepflg
    Wait And Click Element    id=jwl_billingprofilebflg
    Wait And Click Element    id=jwl_billingprofilejflg
    #### User profile data ###
    Wait And Click Element    id=jwl_usr_pro_firstname
    send text to element    id=jwl_usr_pro_firstname_i    ${UPFirst_Name}
    Wait And Click Element    id=jwl_usr_pro_secondname
    send text to element    id=jwl_usr_pro_secondname_i    ${UPSec_Name}
    Wait And Click Element    id=jwl_usr_pro_thirdname
    send text to element    id=jwl_usr_pro_thirdname_i    ${UPThr_Name}
    Wait And Click Element    id=jwl_usr_pro_lastname
    send text to element    id=jwl_usr_pro_lastname_i    ${UPLast_Name}
    wait and click element    id=jwl_usr_pro_fullname
    send text to element    id=jwl_usr_pro_fullname_i    ${UPFull_Name}
    Wait And Click Element    id=jwl_auth_per_signaturename
    send text to element    id=jwl_auth_per_signaturename_i    ${UPAuthSig_Name}
    #Wait And Click Element    id=jwl_usr_pro_birthdate
    #send text to element    id=jwl_usr_pro_birthdate_i    31/01/2018
    Wait And Click Element    id=jwl_usr_pro_gendercode
    Wait And Click Element    xpath=//select[@id='jwl_usr_pro_gendercode_i']//option[contains(text(),'${UP_Gender}')]
    Wait And Click Element    id=jwl_usr_pro_email
    send text to element    id=jwl_usr_pro_email_i    ${UP_Email}
    Wait And Click Element    id=jwl_usr_pro_mainphonenumber
    send text to element    id=jwl_usr_pro_mainphonenumber_i    ${UP_PhoneNum}
    Wait And Click Element    id=jwl_usr_pro_faxnum
    send text to element    id=jwl_usr_pro_faxnum_i    ${UP_FaxNum}
    Wait And Click Element    id=stageAdvanceActionContainer
    Comment    wait for element    id=stageAdvanceActionContainer
    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    Comment    SaveAndClickOnNextStage
    Unselect Frame

VerifyUpdateAccountStatus
    sleep    20s
    ${message}    Confirm Action
    log    ${message}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    wait for element    xpath=//div[@id='header_statuscode']/div/span    40s
    ${StatusReason}    Get Text    xpath=//div[@id='header_statuscode']/div/span
    Run Keyword If    '${StatusReason}'=='SubmittedSuccessfully'    Log    Customer status is ${StatusReason}
    ...    ELSE    Log    Customer status is ${StatusReason}
    Run Keyword If    '${message}'=='BI New Customer Submitted Successfully.'    Log    Customer created succesfully and has name =${CustomerName}
    ...    ELSE    Log    We got error while creating customer. Error detail is =${message}
    close browser

UpdateAccDownloadPage
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame

UpdateAccUploadPage
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame

UpdateAccSummaryPage
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    Unselect Frame

UpdateAccSubmit
    Wait Until Element Is Visible    //li[@id='jwl_bi_updateaccount|NoRelationship|Form|jwl.jwl_bi_updateaccount.Submit.Button']    20s
    Focus    //li[@id='jwl_bi_updateaccount|NoRelationship|Form|jwl.jwl_bi_updateaccount.Submit.Button']
    click on element    //li[@id='jwl_bi_updateaccount|NoRelationship|Form|jwl.jwl_bi_updateaccount.Submit.Button']
    sleep    20s
    ${Updatemessage}    Confirm Action
    sleep    4s
    Comment    wait for element    //li[@id='jwl_bi_updateaccount|NoRelationship|Form|Mscrm.Form.jwl_bi_updateaccount.NewRecord']
    log    ${Updatemessage}
    Run Keyword If    '${Updatemessage}'=='BI Update Account Submitted Successfully.'    Log    Update Account BI sumbitted succesfully
    ...    ELSE    Log    Update Account BI is not sumbitted succesfully Error message =${Updatemessage}

OpenAddressInformation
    Comment    Wait for element    id=crmRibbonManager    40    5
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    sleep    20s
    Unselect Frame
    Select IFrame    id=contentIFrame1
    sleep    20s
    wait and click element    //h2[@id='tab_16_header_h2']
    Unselect Frame
    Select IFrame    id=contentIFrame1
    Sleep    10s
    wait for element    Xpath=.//table[@id='gridBodyTable']/tbody/tr/td[2]/div    40    5
    Double Click Element    Xpath=.//table[@id='gridBodyTable']/tbody/tr/td[2]/div
    Wait And Click Element    Xpath=.//*[@id='etel_customeraddress|NoRelationship|Form|jwl.etel_customeraddress.UpdateAddress.Button']/span    60    5
    sleep    30s
    Comment    wait for element    xpath=.//*[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']/span/    40    2
    Wait And Click Element    //h2[@id='tab_16_header_h2']
    Wait And Click Element    Xpath=.//table[@id='gridBodyTable']/tbody/tr
    Double Click Element    Xpath=.//table[@id='gridBodyTable']/tbody/tr

OpenSubscription
    [Arguments]    ${Subscription_Id}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Comment    sleep    20s
    Run Keyword And Ignore Error    wait and click element    //h2[@id='tab_16_header_h2']
    Run Keyword And Ignore Error    Wait And Click Element    id=tab_13_header_h2
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    Select IFrame    id=contentIFrame1
    wait and click element    //h2[@id='subscriptions_header_h2']
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Comment    Run Keyword And Ignore Error    wait for element    id=Subscription_openAssociatedGridViewImageButtonImage
    Comment    Run Keyword And Ignore Error    click on element    id=Subscription_openAssociatedGridViewImageButtonImage
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    #For Loop will only open provided subscription if it is active##
    : FOR    ${INDEX}    IN RANGE    1    10
    \    Log    ${INDEX}
    \    ${a}    Get Text    xpath=//table[@id='gridBodyTable']//tbody//tr[${INDEX}]//td[5]
    \    ${b}    Get Text    xpath=//table[@id='gridBodyTable']//tbody//tr[${INDEX}]//td[3]
    \    Run Keyword If    '${a}' \ \ == \ \ '${Subscription_Id}' \ and \ \ '${b}' \ \ == \ \ 'Active'    Exit For Loop
    Double Click Element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]/div

OpenModifySubscription
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    wait for element    id=contentIFrame1    60    1
    Select Frame    id=contentIFrame1
    Unselect Frame
    Comment    Sleep    25s
    wait for element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]    60    1
    sleep    3s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']    40    1
    Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.OrderCapture.Button']
    sleep    3s
    click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.OrderCapture.Button']
    Comment    Wait for element until disappear    id=loading    20s    2s
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.Form.etel_subscription.ModifySubscription']
    Comment    sleep    2s
    Click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.Form.etel_subscription.ModifySubscription']
    Unselect Frame

Open Change Ownership
    wait for element    id=contentIFrame1    60    2
    Select Frame    id=contentIFrame1
    Unselect Frame
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']    60    2
    Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Sleep    10s
    click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.OrderCapture.Button']
    click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.OrderCapture.Button']
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Cos.Button']    40    1
    Sleep    10s
    Click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Cos.Button']
    Unselect Frame

Open Reactivate Subscription
    wait for element    id=contentIFrame1
    Select Frame    id=contentIFrame1
    Unselect Frame
    Sleep    25s
    wait for element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.OrderCapture.Button']
    click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.OrderCapture.Button']
    Comment    Wait for element until disappear    id=loading    20s    2s
    sleep    15s
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.reactivateSubscription.Button']
    Click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.reactivateSubscription.Button']
    Unselect Frame

OpenCorrectionOCC
    wait for element    id=contentIFrame1
    Select Frame    id=contentIFrame1
    Unselect Frame
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    sleep    20s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Run Keyword And Ignore Error    wait for element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    Comment    Wait for element until disappear    id=loading    20s    2s
    Comment    sleep    10s
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Button3.Button']
    Click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Button3.Button']
    Unselect Frame

SelectSubWithConID
    [Arguments]    ${Subscription_Id}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Comment    sleep    20s
    Run Keyword And Ignore Error    wait and click element    //h2[@id='tab_16_header_h2']
    Run Keyword And Ignore Error    wait and click element    id=tab_13_header_h2
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    Select IFrame    id=contentIFrame1
    wait and click element    //h2[@id='subscriptions_header_h2']
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    #### Get count of the webtable ###
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to evaluate the correct subscription name ###
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${External_id}=    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]
    \    log    ${External_id}
    \    ${Status}    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody//tr[${INDEX}]//td[3]
    \    ${T1}    Convert To String    ${External_Id}
    \    ${T2}    Convert To String    ${Subscription_Id}
    \    ${a}    Split String    ${T1}    CONTR
    \    ${b}    Split String    ${T2}    CONTR
    \    ${c}    Convert To String    ${b[1]}
    \    log    ${c}
    \    ${d}    Convert To String    ${a[1]}
    \    log    ${d}
    \    Run Keyword If    '${c}'=='${d}' AND '${Status}'=='Active'    Click Element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]
    \    Exit For Loop If    '${c}'=='${d}' AND '${Status}'=='Active'
    ##### For loop ends ####
    Unselect Frame

OpenInvoiceAdjustmentBI-Wrong
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    //a[@class='ms-crm-Menu-Label']//span[contains(text(),' Order Capture ')]
    sleep    10s
    Wait And Click Element    //a[@class='ms-crm-Menu-Label']//span[contains(text(),' New Subscription')]
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=processStepsContainer
    wait for element    id=processStep_corporateCustomer
    Double Click Element    //div[@id='processStep_corporateCustomer']//div[@class='processStepValue']

OpenInvoiceAdjustmentBI
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    sleep    7s
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.InvoiceAdjustment.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.InvoiceAdjustment.Button']
    sleep    2s

OpenFreezeSubscription
    wait for element    id=contentIFrame1
    Select Frame    id=contentIFrame1
    Unselect Frame
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Sleep    10s
    click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.OrderCapture.Button']
    click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.OrderCapture.Button']
    sleep    10s
    Wait And Click Element    //span[contains(text(),'Subscription Status Change')]
    Wait And Click Element    //span[contains(text(),'Freeze Subscription')]

OpenInvoicePaymentBI
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    sleep    7s
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.InvoicePayment.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.InvoicePayment.Button']
    sleep    2s

OpenNonInvoicePaymentBI
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    sleep    7s
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.NonInvoicePayment.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.NonInvoicePayment.Button']
    sleep    2s

OpenBalanceRefillBI
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    2s
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    #Wait And Click Element    id=etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button
    sleep    2s
    Wait And Click Element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BalanceRefill.Button']

OpenVoucherRefillBI
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    2s
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    #Wait And Click Element    id=etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button
    sleep    2s
    Wait And Click Element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.VoucherRefill.Button_1']

OpenSKFPOrder
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.OrderCapture.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.OrderCapture.Button']
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.SalesSKFP.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.SalesSKFP.Button']

OpenNewSubscription
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    //a[@class='ms-crm-Menu-Label']//span[contains(text(),' Order Capture ')]
    sleep    5s
    Wait And Click Element    //a[@class='ms-crm-Menu-Label']//span[contains(text(),' New Subscription')]

SelectSubWithAnyStatus
    [Arguments]    ${Contract_ID}
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    sleep    10s
    Run Keyword And Ignore Error    wait and click element    //h2[@id='tab_24_header_h2']
    Unselect Frame
    Select IFrame    id=contentIFrame1
    wait and click element    //h2[@id='subscriptions_header_h2']
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    #### Get count of the webtable ###
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to evaluate the correct movie name ###
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${External_id}=    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]
    \    log    ${External_id}
    \    ${T1}    Convert To String    ${External_id}
    \    ${T2}    Convert To String    ${Contract_ID}
    \    ${a}    Split String    ${T1}    CONTR
    \    ${b}    Split String    ${T2}    CONTR
    \    ${c}    Convert To String    ${b[1]}
    \    log    ${c}
    \    ${d}    Convert To String    ${a[1]}
    \    log    ${d}
    \    Run Keyword If    '${c}'=='${d}'    Click Element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]
    \    Exit For Loop If    '${c}'=='${d}'
    Wait for element until disappear    //img[@id='loading']    10s    2s
    ##### For loop ends ####
    Unselect Frame

OpenAccHistory
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    sleep    7s
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.AccHistory.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.AccHistory.Button']
    sleep    2s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']

OpenOfferChange
    wait for element    id=contentIFrame1    60    2
    Select Frame    id=contentIFrame1
    Unselect Frame
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']    60    2
    Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Sleep    10s
    click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.OrderCapture.Button']
    click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.OrderCapture.Button']
    Comment    wait for element    //span[contains(text(),'OFFER CHANGE')]    40    1
    Sleep    10s
    Wait And Click Element    //span[contains(text(),'OFFER CHANGE')]
    Unselect Frame

Open FamilyPackage
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']    60    1
    wait for element    id=contentIFrame1    40    1
    Select Frame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']    40    1
    Run Keyword And Ignore Error    wait for element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Sleep    10s
    click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    Wait for element until disappear    id=loading    20s    2s
    sleep    5s
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.FamilyPack.Button']
    Click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.FamilyPack.Button']
    Unselect Frame

Navigate to BulkOrder
    Wait And Click Element    //img[@id='homeButtonImage']
    Wait And Click Element    //a[@id='Settings']
    Wait And Click Element    //a[@id='rightNavLink']
    Wait And Click Element    //a[@id='jwl_bulkoperation']
    Wait And Click Element    //li[@id='jwl_bulkoperation|NoRelationship|HomePageGrid|Mscrm.HomepageGrid.jwl_bulkoperation.NewRecord']

OpenCorpCustomerWithCustID
    [Arguments]    ${CUSTOMER_ID}
    wait for element    id=account|NoRelationship|HomePageGrid|Mscrm.HomepageGrid.account.DeleteMenu    70    1
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    sleep    5s
    wait for element    id=crmGrid_findCriteria
    click on element    id=crmGrid_findCriteria
    send text to element    id=crmGrid_findCriteria    ${CUSTOMER_ID}
    Wait And Click Element    Xpath=.//*[@id='crmGrid_findCriteriaImg']
    Wait And Click Element    //div[@id='crmGrid_divDataArea']//table[@id='gridBodyTable']//tr[@class='ms-crm-List-Row']
    wait for element    //img[starts-with(@id,'gridBodyTable_checkBox_Image')]
    Double Click Element    //img[starts-with(@id,'gridBodyTable_checkBox_Image')]

OpenCorpSubscription
    [Arguments]    ${Subscription_Id}
    wait for element    id=account|NoRelationship|Form|jwl.account.Actions.Button    60    2
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    sleep    10s
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    sleep    20s
    wait and click element    //h2[@id='subscriptions_header_h2']    40    2
    Unselect Frame
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']    40    2
    Comment    Wait And Click Element    Xpath= .//*[@id='gridBodyTable']//div[contains(string(),'CONTR0000001496')]
    Comment    Double Click Element    Xpath= .//*[@id='gridBodyTable']//div[contains(string(),'CONTR0000001496')]
    Comment    wait for element    //div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']/tbody/tr[2]/td[5]/div
    Comment    Double Click Element    //div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']/tbody/tr[2]/td[5]/div
    #For Loop will only open provided subscription if it is active##
    : FOR    ${INDEX}    IN RANGE    1    10
    \    Log    ${INDEX}
    \    ${a}    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]/div
    \    ${b}    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[3]/nobr/span
    \    Run Keyword If    '${a}' \ \ == \ \ '${Subscription_Id}' \ and \ \ '${b}' \ \ == \ \ 'Active'    Exit For Loop
    Double Click Element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]/div

SearchSubscription
    [Arguments]    ${Call_detail_sub}
    Input Text    //input[@id='search']    ${Call_detail_sub}
    Wait And Click Element    //img[@id='findCriteriaImg']
    wait for element    contentIFrame0
    Select IFrame    contentIFrame0
    Wait And Click Element    //span[@id='attribtwo']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
