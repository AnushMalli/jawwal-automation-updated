*** Settings ***
Resource          ../Utils/NavigateFunctions/Navigator.robot
Resource          ../Facilities/BaseCustomerFacilities.robot
Library           ExtendedSelenium2Library
Resource          CustomerCreate.robot
Library           ../CustomLibrary/UpdateExcelSheet.py
Resource          ../Tests/TestData.txt

*** Keywords ***
StartBISafely
    Select IFrame    id=contentIFrame1
    Select IFrame    id=WebResource_biheader
    Wait And Click Element    id=stop
    Sleep    5s
    Wait And Click Element    id=start
    Unselect Frame
    Unselect Frame

SelectSubsInIndvCustPage
    Select IFrame    id=contentIFrame1
    Wait And Click Element    xpath=//div[@id='Subscription_divDataArea']/div/table/tbody/tr/td[4]
    Unselect Frame

SelectSubsInCorpCustPage
    Select IFrame    id=contentIFrame1
    Sleep    15s
    Scroll Element Into View    id=subscriptions_header_h2
    Click Element    id=subscriptions_header_h2
    Wait And Click Element    xpath=//div[@id='Subscription_divDataArea']/div/table/tbody/tr/td[2]
    Unselect Frame

GoToSubsDetailForSelectedIndv
    Select IFrame    id=contentIFrame1
    Sleep    15s
    Scroll Element Into View    id=subscriptions_header_h2
    Click Element    id=subscriptions_header_h2
    Wait Until Angular Ready
    Double Click Element    Xpath=//div[@id='Subscription_divDataArea']/div/table/tbody/tr/td[2]
    Sleep    10s
    Wait And Click Element    Xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']/span/a/span
    Wait And Click Element    Xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']/span/a/span
    Unselect Frame

GoToSubsDetailForSelectedCorp
    Select IFrame    id=contentIFrame1
    Scroll Element Into View    id=Subscription_divDataArea
    Wait And Click Element    xpath=//div[@id='Subscription_divDataArea']/div/table/tbody/tr[4]/td[2]/nobr/a
    Unselect Frame

ChangeLanguage
    Sleep    10s
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=etel_languagecode
    Wait And Click Element    xpath=//select[@id='etel_languagecode_i']/option[3]
    Wait And Click Element    id=savefooter_statuscontrol
    Unselect Frame

ClickStartBI
    Select IFrame    id=contentIFrame1
    Select IFrame    id=WebResource_biheader
    Wait And Click Element    id=start
    Unselect Frame
    Unselect Frame

Change GenralInfo
    Select IFrame    id=contentIFrame1
    Wait And Click Element    xpath=//*[@id="{0c4db541-d4a7-7326-3e2b-65bdba4ca80c}_Expander"]
    #wait and click Element    css=div.ms-crm-Inline-Value.ms-crm-Inline-EditHintState
    click on element    xpath=//*[@id="jwl_prepaidflag"]/div
    Unselect Frame

Change CustomerInfo
    Sleep    10s
    Select IFrame    id=contentIFrame1
    Wait And Click Element    xpath=//*[@id="{6ea626a5-453c-3799-575b-94b9853ad9ca}_Expander"]
    Wait And Click Element    xpath=//*[@id="jwl_prepaidflag"]/div[1]/span
    Capture Page Screenshot
    Unselect Frame

SaveChanges
    Select IFrame    id=contentIFrame1
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    Unselect Frame

SaveAndClickOnNextStage
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    Comment    wait and click element    xpath=//div[@id='stageAdvanceActionContainer']/div
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame

PaymentAndSummaryPage
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    wait and click element    id=savefooter_statuscontrol
    Wait and click element    css=div.stageActionText
    wait and click element    id=savefooter_statuscontrol
    Unselect Frame

SubmitCustomer
    wait for element    xpath=//li[@id='jwl_bi_newcustomer|NoRelationship|Form|jwl.jwl_bi_newcustomer.Button1.Button']//span[contains(text(),'Submit')]    40s
    Sleep    5s
    click on element    xpath=//li[@id='jwl_bi_newcustomer|NoRelationship|Form|jwl.jwl_bi_newcustomer.Button1.Button']//span[contains(text(),'Submit')]

VerifyTheSubmittedCustomerStatus
    [Arguments]    ${CustomerName}
    sleep    30s
    ${message}    Confirm Action
    log    ${message}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    wait for element    xpath=//div[@id='header_statuscode']/div/span    40s
    sleep    10s
    ${StatusReason}    Get Text    xpath=//div[@id='header_statuscode']/div/span
    Run Keyword If    '${StatusReason}'=='SubmittedSuccessfully'    Log    Customer status is ${StatusReason}
    ...    ELSE    Log    Customer status is ${StatusReason}
    Run Keyword If    '${message}'=='BI New Customer Submitted Successfully.'    Log    Customer created succesfully and has name =${CustomerName}
    ...    ELSE    Log    We got error while creating customer. Error detail is =${message}
    ${CUSTOMER_ID}    Run Keyword If    '${StatusReason}'=='SubmittedSuccessfully'    GetCustomerNumFromCustName    ${CustomerName}
    Write Excel    ${FilePath}/InputData.xls    0    1    0    ${CUSTOMER_ID}
    Write Excel    ${FilePath}/InputData.xls    0    1    1    ${CustomerName}
    Write Excel    ${FilePath}/InputData.xls    1    1    0    ${CUSTOMER_ID}

OpenSelectedSubscriptionBasedOnContractID
    Select IFrame    id=contentIFrame1
    Sleep    120s
    #Scroll Element Into View    id=subscriptions_header_h2
    #Wait And Click Element    id=subscriptions_header_h2
    wait and click element    //h2[@id='subscriptions_header_h2']
    Wait Until Angular Ready
    Wait And Click Element    Xpath= .//*[@id='gridBodyTable']//div[contains(string(),'CONTR0000001338')]
    #Double Click Element    Xpath=.//*[@id='gridBodyTable']/tbody/tr[1]/td[5]/div
    #Wait And Click Element    Xpath= .//*[@id='gridBodyTable']//div[contains(string(),'CONTR0000001338')]
    Double Click Element    Xpath= .//*[@id='gridBodyTable']//div[contains(string(),'CONTR0000001338')]
    Unselect Frame

UpdateAddress
    wait for element    id=commandContainer4    60    5
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    wait for element    id=contentIFrame0
    Comment    Select IFrame    id=contentIFrame0
    Comment    Wait And Click Element    xpath=//div[@id='jwl_address1']/div    60    5
    Sleep    20s
    wait for element    //td[@id='jwl_address_line1_d']
    Wait And Click Element    //div[@id='jwl_address_line1']
    send text to element    id=jwl_address_line1_i    Istanbul
    Wait And Click Element    xpath=//div[@id='footer_statuscontrol']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@id='footer_statuscontrol']//img[@id='savefooter_statuscontrol']
    Unselect Frame
    Sleep    15s
    Wait And Click Element    Xpath=//li[@id='jwl_bi_customeraddressupdate|NoRelationship|Form|jwl.jwl_bi_customeraddressupdate.Button1.Button']/span
