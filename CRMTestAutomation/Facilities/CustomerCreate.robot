*** Settings ***
Resource          ../Utils/NavigateFunctions/Navigator.robot
Resource          ../Facilities/BaseCustomerFacilities.robot
Library           ExtendedSelenium2Library
Resource          LibraryFileData.txt
Library           FakerLibrary
Library           DateTime

*** Keywords ***
ClickNewIndividual
    Wait And Click Element    xpath=//*[@id="navTabGlobalCreateImage"]
    Wait And Click Element    xpath=//*[@id="{608861bc-50a4-4c5f-a02c-21fe1943e2cf}"]/span[2]
    #Unselect Frame

CreateIndividualForm
    ##### Navigate to the frame where we will insert all the manditory parameters ####
    Select IFrame    id=NavBarGloablQuickCreate
    ##### First Name Data ####
    Wait And Click Element    xpath=//div[@id='firstname']/div
    ${FirstName}    FakerLibrary.First Name Male
    send text to element    id=firstname_i    ${FirstName}
    Wait And Click Element    id=lastname
    ##### Last Name Data ####
    ${LastName}    FakerLibrary.Last Name Male
    send text to element    id=lastname_i    ${LastName}
    ##### ID Type ###
    Wait And Click Element    //div[@id='jwl_idtypeindividual']/div
    click on element    xpath=//select/option[contains(text(),'${ID_Type}')]
    Wait And Click Element    //div[@id='governmentid']/div
    ${ID_Type_Value}    FakerLibrary.Random Int
    send text to element    id=governmentid_i    ${ID_Type_Value}
    Wait And Click Element    //div[@id='jwl_accounttype']/div
    #Select From List By Label    id=jwl_accounttype_i    label=Individual
    click on element    xpath=//select/option[contains(text(),'${Customer_category}')]
    Unselect Frame
    wait for element    id=globalquickcreate_save_button_NavBarGloablQuickCreate
    focus    id=globalquickcreate_save_button_NavBarGloablQuickCreate
    click on element    id=globalquickcreate_save_button_NavBarGloablQuickCreate
    Comment    Close All Browsers
    [Return]    ${FirstName}

ClickNewCorporate
    Wait And Click Element    xpath=//*[@id="navTabGlobalCreateImage"]
    Wait And Click Element    xpath=//a[@id='{70816501-edb9-4740-a16c-6a5efbc05d84}']/span[2]

CreateCorporateForm
    #### Select Frame to enter details ###
    Select IFrame    id=NavBarGloablQuickCreate
    #### Enter Corporate Name ####
    Wait And Click Element    xpath=//div[@id='name']/div
    ${CorporateName}    FakerLibrary.Company
    send text to element    id=name_i    ${CorporateName}
    #### Select Corporate from "Customer Category" ###
    Wait And Click Element    css=input#jwl_firstname_i
    click on element    xpath=//select[@id='jwl_accounttype_i']/option[contains(text(),'${Corp_Customer_Category}')]
    #Wait And Click Element    id=jwl_accounttype_cl
    #Wait And Click Element    //option[@value='1']
    #### Select ID type    ####
    Wait And Click Element    //div[@id='jwl_idtypecorporate']/div/span
    click on element    xpath=//select/option[contains(text(),'${Corp_ID_Type}')]
    ##### Enter random geneated ID ###
    ${CorpID_Type_Value}    FakerLibrary.Random Int
    click on element    //div[@id='etel_taxnumber']/div
    send text to element    id=etel_taxnumber_i    ${CorpID_Type_Value}
    ### Select communication method ####
    Wait and click Element    //div[@id='jwl_preferredchannel']/div
    click on element    xpath=//select/option[contains(text(),'${Corp_Commu_method}')]
    Unselect Frame
    #### save the changes ###
    wait for element    id=globalquickcreate_save_button_NavBarGloablQuickCreate
    focus    id=globalquickcreate_save_button_NavBarGloablQuickCreate
    click on element    id=globalquickcreate_save_button_NavBarGloablQuickCreate
    #
    #Wait And Click Element    xpath=(//option[@value='1'])[2]
    #Wait And Click Element    //button[@id='globalquickcreate_save_button_NavBarGloablQuickCreate']
    [Return]    ${CorporateName}

VerifyNewCustomerCreated
    [Arguments]    ${Name}
    Wait And Click Element    //div[@id='navStatusArea']/table/tbody/tr/td[3]/a/span
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    ${pass}    Run Keyword And Return Status    Element Should Be Visible    xpath=//h1[contains(text(),'${Name}')]
    Run Keyword If    ${pass}    log    Individual customer is created succesfully
    ...    ELSE    Individual customer is not created succesfully
    Unselect Frame

NewCustomerBICustomerInfoPage
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    click on element    xpath=//a[@id='btnIndividualCustomerAcquisition']/span/span/span
    Unselect Frame
    #wait for element    id=contentIFrame0
    #wait for element    //div[@id='stage_0']/div[2]/div/div/div/span
    Select IFrame    id=contentIFrame0
    ##### First Name Data ####
    wait for element    id=jwl_firstname
    Wait Until Element Is Visible    id=jwl_firstname    40s
    click on element    id=jwl_firstname
    ${FirstName}    FakerLibrary.First Name Male
    send text to element    id=jwl_firstname_i    ${FirstName}
    ### Last name data ####
    Wait And Click Element    id=jwl_lastnamecompanyname
    ${LastName}    FakerLibrary.Last Name Male
    send text to element    id=jwl_lastnamecompanyname_i    ${LastName}
    ###### customer category ####
    wait and click element    id=jwl_customercategory
    #Select From List By Label    id=jwl_customercategory_i    label=Individual
    click on element    xpath=//select[@id='jwl_customercategory_i']/option[@title='Individual']
    ##### ID Type ###
    Wait And Click Element    id=jwl_idtypeid
    Comment    wait and click element    id=jwl_idtypeid_i
    Wait And Click Element    id=Dialog_jwl_idtypeid_i_IMenu
    Comment    wait and click element    xpath=//select[@id='jwl_idtype_i']/option[@title='Passport Number']
    ##### ID Type value ###
    Wait And Click Element    id=jwl_idnumber
    Wait Until Element Is Visible    id=jwl_idnumber    20s
    ${ID_Type_Value}    FakerLibrary.Random Int
    Input Text    xpath=//input[@id='jwl_idnumber_i']    ${ID_Type_Value}
    ####Language ###
    Wait And Click Element    id=jwl_languagecode
    click on element    xpath=//select[@id='jwl_languagecode_i']/option[@title='English']
    ####Main phone number####
    Wait And Click Element    id=jwl_mainphone
    ${a}    Generate Random String    8    [NUMBERS]
    log    ${a}
    ${MobileNumber} =    Catenate    SEPARATOR=    0    ${a}
    send text to element    id=jwl_mainphone_i    ${MobileNumber}
    Wait And Click Element    id=jwl_placeofwork
    ${placeofwork}    FakerLibrary.City
    send text to element    id=jwl_placeofwork_i    ${placeofwork}
    Wait And Click Element    id=jwl_placeofbirth
    ${placeofbirth}    FakerLibrary.City
    send text to element    id=jwl_placeofbirth_i    ${placeofbirth}
    ### Customer Type ###
    Wait And Click Element    id=jwl_profileclassificationid
    Wait And Click Element    css=img.ms-crm-IL-MenuItem-Icon.ms-crm-IL-MenuItem-Icon-Hover
    click on element    id=savefooter_statuscontrol
    wait for element    id=savefooter_statuscontrol
    wait and click element    id=stageAdvanceActionContainer
    Unselect Frame
    [Return]    ${FirstName}

NewCustomerBIAddressInfoPage
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait And Click Element    xpath=//div[@id='jwl_address1']/div
    ${address1}    FakerLibrary.Address
    send text to element    id=jwl_address1_i    ${address1}
    Wait And Click Element    xpath=//div[@id='jwl_address2']/div
    ${address2}    FakerLibrary.Address
    send text to element    id=jwl_address2_i    ${address2}
    Wait And Click Element    xpath=//div[@id='jwl_address3']/div
    ${address3}    FakerLibrary.Address
    send text to element    id=jwl_address3_i    ${address3}
    wait and click element    id=stageAdvanceActionContainer
    Comment    SaveAndClickOnNextStage
    Unselect Frame

NewCustomerBICorpCustomerInfoPage
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    click on element    id=btnCorporateCustomerCreation
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Sleep    20s
    Wait and Click Element    id=jwl_lastnamecompanyname    timeout=40 sec
    ${LastName}    FakerLibrary.Last Name Male
    send text to element    id=jwl_lastnamecompanyname_i    ${LastName}
    Wait and Click Element    id=jwl_shortname
    ${FirstName}    FakerLibrary.First Name Male
    send text to element    id=jwl_shortname_i    ${FirstName}
    Wait and Click Element    id=jwl_idtypeid
    #wait for element    id=Dialog_jwl_idtypeid_i_IMenu
    Sleep    5s
    Wait And Click Element    Xpath=//ul[@id='jwl_idtypeid_i_IMenu']/li[2]/a[2]/span/nobr/span
    #Press Key    id=Dialog_jwl_idtypeid_i_IMenu
    #Wait and Click Element    id=jwl_idnumber
    #{ID_Type_Value}    FakerLibrary.Random Int
    #send text to element    id=jwl_idnumber_i    ${ID_Type_Value}
    Wait and Click Element    id=jwl_taxoffice
    Input Text    id=jwl_taxoffice_i    Istanbul
    Wait and Click Element    id=jwl_idnumber
    ${ID_Type_Value}    FakerLibrary.Random Int
    send text to element    id=jwl_idnumber_i    ${ID_Type_Value}
    Wait and Click Element    id=jwl_mainphone
    ${a}    Generate Random String    8    [NUMBERS]
    log    ${a}
    ${MobileNumber} =    Catenate    SEPARATOR=    0    ${a}
    send text to element    id=jwl_mainphone_i    ${MobileNumber}
    Wait and Click Element    id=jwl_natureofbusiness
    click on element    Xpath=//select[@id='jwl_natureofbusiness_i']/option[@title='Other']
    Wait And Click Element    id=jwl_servicecenter
    Input Text    id=jwl_servicecenter_i    Istanbul
    Wait And Click Element    id=jwl_dateofcontract
    ${date1}    DateTime.Get Current Date    time_zone=local    increment=0    result_format=%d-%m-%Y
    send text to element    id=DateInput    ${date1}
    #Wait And Click Element    id=jwl_profileclassificationid
    #click on element    Xpath=//div[@id='Dialog_jwl_profileclassificationid_i_IMenu']/div/u1/li/a[@title='CC']
    Wait And Click Element    id=jwl_profileclassificationid
    wait for element    id=Dialog_jwl_profileclassificationid_i_IMenu
    Press Key    id=Dialog_jwl_profileclassificationid_i_IMenu    \\13
    Wait And Click Element    id=jwl_languagecode
    click on element    Xpath=//select[@id='jwl_languagecode_i']/option[@title='English']
    Wait And Click Element    id=jwl_linepayment
    sleep    5s
    click on element    Xpath=//select[@id='jwl_linepayment_i']/option[@title='SUBSCRIBER HIM SELF']
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame

NewCustomerBICorpAddressContactInfoPage
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait And Click Element    id=jwl_address1
    ${address1}    FakerLibrary.Address
    send text to element    id=jwl_address1_i    ${address1}
    Wait And Click Element    xpath=//div[@id='jwl_address2']/div
    ${address2}    FakerLibrary.Address
    send text to element    id=jwl_address2_i    ${address2}
    Wait And Click Element    id=jwl_address3
    ${address3}    FakerLibrary.Address
    send text to element    id=jwl_address3_i    ${address3}
    Wait And Click Element    id=jwl_contactfirstname
    ${FirstName}    FakerLibrary.First Name Male
    send text to element    id=jwl_contactfirstname_i    ${FirstName}
    Wait And Click Element    id=jwl_contactlastname
    ${LastName}    FakerLibrary.First Name Male
    send text to element    id=jwl_contactlastname_i    ${LastName}
    Wait and Click Element    id=jwl_contactidtypenewid
    wait for element    id=Dialog_jwl_contactidtypenewid_i_IMenu
    Sleep    10s
    Wait And Click Element    Xpath=//ul[@id='jwl_contactidtypenewid_i_IMenu']/li[2]/a[2]/span/nobr/span
    Wait and Click Element    id=jwl_contactidnumber
    ${ID_Type_Value}    FakerLibrary.Random Int
    send text to element    id=jwl_contactidnumber_i    ${ID_Type_Value}
    Wait And Click Element    id=jwl_contactgender
    click on element    Xpath=//select[@id='jwl_contactgender_i']/option[@title='Male']
    Wait And Click Element    id=jwl_contactbirthdate
    #send text to element    id=jwl_contactbirthdate_i    09/01/1988
    ${date1}    DateTime.Get Current Date    time_zone=local    increment=0    result_format=%d-%m-%Y
    send text to element    Xpath=//table[@id='jwl_contactbirthdate_i']/tr/td/input    ${date1}
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame

GetCustomerNumFromCustName
    [Arguments]    ${CustomerName}
    GoToIndividualCustomersList
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    sleep    5s
    wait for element    id=crmGrid_findCriteria
    click on element    id=crmGrid_findCriteria
    send text to element    id=crmGrid_findCriteria    ${CustomerName}
    Wait And Click Element    Xpath=.//*[@id='crmGrid_findCriteriaImg']
    wait for element    //table[@id='gridBodyTable']
    ${CustomerNameFromWeb}    Get text    //a[starts-with(@id,'gridBodyTable_primaryField')]
    ${match1}    ${value1}    Run Keyword And Ignore Error    Should Contain    ${CustomerNameFromWeb}    ${CustomerName}
    ${ReturnResult}    Set Variable If    '${match1}'=='PASS'    ${True}    ${False}
    ${CUSTOMER_ID}    Run Keyword If    '${ReturnResult}'=='True'    Get Text    //table[@id='gridBodyTable']//tbody//tr//td[6]
    log    ${CUSTOMER_ID}
    [Return]    ${CUSTOMER_ID}
