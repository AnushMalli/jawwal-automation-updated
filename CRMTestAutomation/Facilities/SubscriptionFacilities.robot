*** Settings ***
Resource          ../Utils/NavigateFunctions/Navigator.robot
Resource          ../Facilities/BaseCustomerFacilities.robot
Library           ExtendedSelenium2Library
Library           FakerLibrary
Library           DateTime
Library           ../CustomLibrary/UpdateExcelSheet.py
Resource          LibraryFileData.txt

*** Keywords ***
OpenAccHistoryFromSubsPage
    ClickMoreCommand
    Wait And Click Element    ${OpenAccountHistoryButtonSubs}

OpenCallDetailStatement
    ClickMoreCommand
    Wait And Click Element    id=etel_subscription|NoRelationship|Form|jwl.etel_subscription.CallDetailStatement.Button

SubmitCorrectionOCCWithoutApproval
    [Arguments]    ${Name}    ${Amount}
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=commandContainer4
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=jwl_name_d
    send text to element    id=jwl_name_i    ${Name}
    Wait And Click Element    id=jwl_amount
    send text to element    id=jwl_amount_i    ${Amount}
    Wait And Click Element    id=jwl_startdateofcorrection
    ${date1}    DateTime.Get Current Date    time_zone=local    increment=0    result_format=%d-%m-%Y
    send text to element    id=DateInput    ${date1}
    Wait And Click Element    id=jwl_noofcycle
    send text to element    id=jwl_noofcycle_i    1
    Wait And Click Element    id=jwl_type
    Wait And Click Element    Xpath=.//*[@id='jwl_type_i']/option[2]
    Wait And Click Element    id=jwl_typereason
    Wait And Click Element    Xpath=.//*[@id='jwl_typereason_i']/option[2]
    Wait And Click Element    id=savefooter_statuscontrol
    wait for element    id=savefooter_statuscontrol
    Unselect Frame
    wait for element    //li[@id='jwl_bi_applycorrectionocc|NoRelationship|Form|jwl.jwl_bi_applycorrectionocc.Submit.Button']
    click on element    id=jwl_bi_applycorrectionocc|NoRelationship|Form|jwl.jwl_bi_applycorrectionocc.Submit.Button
    Comment    wait for element    //li[@id='jwl_bi_applycorrectionocc|NoRelationship|Form|jwl.jwl_bi_applycorrectionocc.Submit.Button']
    Comment    Sleep    15s
    Comment    Wait And Click Element    Xpath=//li[@id='jwl_bi_applycorrectionocc|NoRelationship|Form|jwl.jwl_bi_applycorrectionocc.Submit.Button']/span
    Comment    Sleep    25s
    Comment    Confirm Action
    #Element Should Contain    Xpath=.//*[@id='header_statuscode']/div[1]/span    Status Reason Submitted Successfully

SubmitCorrectionOCCWithApproval
    wait for element    id=commandContainer4
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    ##Enetring Data##
    Wait And Click Element    id=jwl_name_d
    send text to element    id=jwl_name_i    test
    Wait And Click Element    id=jwl_amount
    send text to element    id=jwl_amount_i    15
    Wait And Click Element    id=jwl_startdateofcorrection
    ${date1}    DateTime.Get Current Date    time_zone=local    increment=0    result_format=%d-%m-%Y
    send text to element    id=DateInput    ${date1}
    Wait And Click Element    id=jwl_noofcycle
    send text to element    id=jwl_noofcycle_i    1
    Wait And Click Element    id=jwl_type
    Wait And Click Element    Xpath=.//*[@id='jwl_type_i']/option[2]
    Wait And Click Element    id=jwl_typereason
    Wait And Click Element    Xpath=.//*[@id='jwl_typereason_i']/option[2]
    ##saving and submitting##
    Wait And Click Element    id=savefooter_statuscontrol
    wait for element    id=savefooter_statuscontrol
    Unselect Frame
    Sleep    15s
    Wait And Click Element    Xpath=//li[@id='jwl_bi_applycorrectionocc|NoRelationship|Form|jwl.jwl_bi_applycorrectionocc.Submit.Button']/span
    Sleep    25s
    Confirm Action
    wait for element    id=contentIFrame0
    ##Approval Level1##
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait And Click Element    Xpath=//table[@id='gridBodyTable']//a[@title='Open Approval Step 1']
    Unselect Frame
    wait for element    xpath=//li[@id='jwl_approvalstep|NoRelationship|Form|jwl.jwl_approvalstep.ApprovalControllerApprove.Button']/span
    Wait And Click Element    xpath=//li[@id='jwl_approvalstep|NoRelationship|Form|jwl.jwl_approvalstep.ApprovalControllerApprove.Button']/span
    Sleep    15s
    Confirm Action
    Sleep    10s
    Wait And Click Element    id=closeButton
    ##Approval Level2##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait And Click Element    Xpath=//table[@id='gridBodyTable']//a[@title='Open Approval Step 2']
    wait for element    xpath=//li[@id='jwl_approvalstep|NoRelationship|Form|jwl.jwl_approvalstep.ApprovalControllerApprove.Button']/span
    Wait And Click Element    xpath=//li[@id='jwl_approvalstep|NoRelationship|Form|jwl.jwl_approvalstep.ApprovalControllerApprove.Button']/span
    Sleep    15s
    Confirm Action
    Sleep    10s
    Wait And Click Element    id=closeButton

ModifySubscription
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Comment    Sleep    25s
    wait for element    //div[@id='existingOfferingsGridBody']
    ${DeactServiCeFromWeb}    Get Text    //div[@id='existingOfferingsGridBody']//table//tbody//tr[contains(string(),'Jordan International Dialling')]//td[1]
    log    ${DeactServiCeFromWeb}
    ${present}    Run Keyword And Return Status    Element Should Be Visible    //div[@id='existingOfferingsGridBody']//table//tbody//tr[contains(string(),'Jordan International Dialling')]
    Run Keyword If    ${present}    Click Element    //div[@id='existingOfferingsGridBody']//table//tbody//tr[contains(string(),'Jordan International Dialling')]//input
    ...    ELSE    log    GRPS services is not available
    Comment    Wait And Click Element    //div[@id='existingOfferingsGridBody']//input[@title='Change Status']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    sleep    5s
    Wait And Click Element    //div[@id='productStatusChangeWidget']//button[contains(text(),'Done')]
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    Sleep    10s
    Wait And Click Element    //div[@id='existingproductsTitle']//span[contains(text(),'Add new Products')]
    Sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${servicetobeadded}    Get Text    //table[@id='configDataGridBody']//tbody//tr[1]//td[1]//div
    log    ${servicetobeadded}
    Wait And Click Element    //table[@id='configDataGridBody']//tbody//tr[1]
    Wait And Click Element    //div[@id='newProductsArea']//span[contains(text(),'ADD TO BASKET')]
    sleep    4s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    Wait And Click Element    //table[@id='msdp_rbtServicesDataGridBody']//span[text(),'Eschools']
    Comment    Wait And Click Element    //div[@id='msdp_rbtProductsArea']//span[text(),'ADD TO BASKET']
    Comment    Sleep    40s
    ${countofrows}    Get Matching Xpath Count    //div[@id='orderBasket']//div[@class='dataGridBodyScroll']//table//tbody//td[1]
    ${countforloop}    Evaluate    ${countofrows}+3
    : FOR    ${INDEX}    IN RANGE    2    ${countforloop}
    \    ${offerfromorderbasket}    Get text    //div[@id='orderBasket']//div[@class='dataGridBodyScroll']//table//tbody//td[1]//div//span[${INDEX}]
    \    ${StatusInOrder}    Run keyword if    '${DeactServiCeFromWeb}'=='${offerfromorderbasket}'    Get Text    //div[@id='orderBasket']//table//tbody//td[3]//div//span[@title='Deactivation']
    \    Run Keyword If    '${StatusInOrder}'=='Deactivation'    log    Service is deactivated succesfully
    \    ...    ELSE    log    Service is not deactivated succesfully
    \    Exit For Loop If    '${DeactServiCeFromWeb}'=='${offerfromorderbasket}'
    ${countofrowsfornewoffer}    Get Matching Xpath Count    //div[@id='orderBasket']//div[@class='dataGridBodyScroll']//table//tbody//td[1]
    ${countforloop1}    Evaluate    ${countofrowsfornewoffer}+3
    : FOR    ${INDEX}    IN RANGE    2    ${countforloop}
    \    ${offerfromorderbasket1}    Get text    //div[@id='orderBasket']//table//tbody//td[1]//div//span[@title='${servicetobeadded}']
    \    Run Keyword If    '${offerfromorderbasket1}'=='${servicetobeadded}'    log    correct offer has been added in order box
    \    ...    ELSE    log    Correct offer is not added, the added offer is :- ${servicetobeadded}
    \    Exit For Loop If    '${offerfromorderbasket1}'=='${servicetobeadded}'
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Configuration##
    Sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Resources##
    Comment    Sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Addon Expiration##
    Comment    Sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Guarantees##
    Comment    Sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Payment##
    Comment    Sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Summary##
    Comment    Sleep    20s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='summaryResourcesContainer']//table//tbody//td//input
    ${SubDate}    DateTime.Get Current Date    time_zone=local    increment=0    result_format=%d-%m-%Y
    send text to element    //div[@id='summaryResourcesContainer']//table//tbody//td//input    ${SubDate}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    id=stageAdvanceActionContainer
    sleep    30s
    ${SumMesg}    Confirm Action
    log    ${SumMesg}
    sleep    2M
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//div[@id='header_jwl_status']    40s
    ${StatusReasonNewSub}    Get Text    xpath=//div[@id="header_jwl_status"]//div//span
    ${match}    Set Variable    ${StatusReasonNewSub}
    ${value}    Set Variable    Submitted
    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${match}    ${value}
    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    log    ${RETURNVALUE}
    Run Keyword If    '${RETURNVALUE}'=='True'    Log    Add/Remove submitted succesfully and status is ${StatusReasonNewSub}
    ...    ELSE    Log    Add/Remove    not submitted succesfully and status is ${StatusReasonNewSub}
    Comment    Wait And Click Element    //div[@id='summaryResourcesContainer']//span[@class='k-icon k-i-calendar']
    Comment    Press Key    //div[@id='summaryResourcesContainer']//span[@class='k-icon k-i-calendar']    \\13
    Comment    Comment    Wait And Click Element    //div[@id='processActionsContainer']//div[@title='Submit']
    Unselect Frame

OpenNewSubAndSelectProduct
    [Arguments]    ${Offer}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    //a[@class='ms-crm-Menu-Label']//span[contains(text(),' Order Capture ')]
    sleep    10s
    Wait And Click Element    //a[@class='ms-crm-Menu-Label']//span[contains(text(),' New Subscription')]
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=newCustomerSearchButtonGroup
    Wait And Click Element    xpath=//div[@id='newCustomerSearchButtonGroup']//button[contains(text(),'SEARCH')]
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //div[@class='dataGridWrapper']
    Wait And Click Element    xpath=//table//tbody//tr[@class='dataGridBodyRow ng-scope']//div[@class='dataGridCellContent']//span[contains(text(),'${Offer}')]
    wait and click element    xpath=//span[@class='cmdBarBtnSpan ng-binding'][@ng-click='!rootScopeData.currentStage.isActive || scopeData.planSelection.addBasket()']
    Unselect Frame
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=orderBasket
    ${alloffers}    Get Text    xpath=//div[@id='orderBasket']//div[@class='dataGridBodyScroll']//tr[@class='dataGridBodyRow ng-scope']//td[1]//span[@class='lowerCellSection ng-binding']
    log    ${alloffers}
    Run Keyword If    '${alloffers}'=='${Offer}'    Log    Correct offer is added in Basket
    ...    ELSE    log    Offer is not added in bascket. Offer in bascket is = ${alloffers}
    Comment    ${a}    Get Text    xpath=//div[@id='right']//div[@id='orderBasket']//div[@class='dataGridBodyScroll']//tr[@class='dataGridBodyRow ng-scope']//td[1]//span[@class='lowerCellSection ng-binding'][@title='Hala 150']
    Comment    log    ${a}
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Comment    wait and click element    xpath=//div[@id='stageAdvanceActionContainer']/div
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame

NewSubConfigurationPage
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    ## wait and click on "Save" ##
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    ## wait and click on "Next Stage" ##
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame

NewSubResoursePage
    [Arguments]    ${MSISDN}    ${SIM}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=resourcesBlock
    wait for element    //input[@id='txtMSISDN']
    click on element    //input[@id='txtMSISDN']
    send text to element    //input[@id='txtMSISDN']    ${MSISDN}
    click on element    //input[@id='txtSIM']
    wait for element    //button[@id='btnAssignMSISDN']
    click on element    //button[@id='btnAssignMSISDN']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${MSISDNAssignmessage}    Confirm Action
    log    ${MSISDNAssignmessage}
    Log To Console    ${MSISDNAssignmessage}
    Run Keyword If    '${MSISDNAssignmessage}'=='MSISDN Reserve completed successfully'    Log    MSISDN Reservered successfully
    ...    ELSE    Log    We got error while reserving the MSISDN. Error detail is =${MSISDNAssignmessage}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    ### SIM Reservation ###
    wait for element    //input[@id='txtSIM']
    send text to element    //input[@id='txtSIM']    ${SIM}
    click on element    //input[@id='txtMSISDN']
    Comment    click on element    //button[@id='btnAssignSIM']
    Comment    sleep    40s
    Comment    ${SIMAssignmessage}    Confirm Action
    Comment    log    ${SIMAssignmessage}
    Comment    Log To Console    ${SIMAssignmessage}
    Comment    Run Keyword If    '${SIMAssignmessage}'=='Sim has reserved successfully'    Log    SIM Reservered successfully
    ...    ELSE    Log    We got error while reserving the SIM. Error detail is =${SIMAssignmessage}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    #### Save changes
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    ### Click on "Next Stage"##
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame

NewSubAddOnPage
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame

NewSubUserProfilePage
    [Arguments]    ${User_profile_Language}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    ## Select language in user profile page###
    wait for element    //div[@id='left']//select[@name='language']
    wait and click element    //div[@id='left']//select[@name='language']//option[contains(text(),'${User_profile_Language}')]
    Unselect Frame
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    ## Save changes##
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    ## Click on "Next Stage"###
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame

NewSubGuaranteePage
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    ## wait and click on "Save" ##
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    ## wait and click on "Next Stage" ##
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame

NewSubFinancialAndPaymentPage
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    ## wait and click on "Save" ##
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    ## wait and click on "Next Stage" ##
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame

NewSubSummaryPage
    [Arguments]    ${Summary_Billing_Lan}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    ## Select Billing language in Summary page###
    wait for element    //table[@id='formField_noSubscriptions']//select//option[@label='English']
    wait and click element    //table[@id='formField_noSubscriptions']//select//option[@label='${Summary_Billing_Lan}']
    Unselect Frame
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    ## Save changes##
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    ## Click on "Next Stage"###
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame

ChangeOwnership-JoinAccount
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //select[@ng-change='changeCustomerType()']    60    2
    Wait And Click Element    //select[@ng-change='changeCustomerType()']//option[contains(text(),'Individual')]
    Wait And Click Element    id=selectSearchTypesId
    Wait And Click Element    //select[@id='selectSearchTypesId']//option[contains(text(),'National Id')]
    Wait And Click Element    id=param1Id
    send text to element    id=param1Id    TC123123
    Comment    Wait And Click Element    id=param2Id
    Comment    send text to element    id=param2Id    Tufan
    Wait And Click Element    id=btn_searchCustomer
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    sleep    10s
    Wait And Click Element    //div[@id='existingOfferingsGridBody']//input[@type='checkbox']
    Sleep    10s
    Wait Until Page Contains Element    //div[@id='processControlCollapsibleArea']//span[contains(text(),'TC123123')]
    ${msisdn}    Get Text    //div[@id='processStepsContainer']/div[1]//div[@class='process_accountNumber']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Log    ${msisdn}
    ${name}    Get Text    //div[@id='processStepsContainer']/div[2]//div[@class='process_accountNumber']//div[@class='processStepValueText processStepLookupValueText']
    Log    ${name}
    Comment    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    Unselect Frame
    ##Open Invoices##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    //div[@class='wizard-body ng-scope']//p[starts-with(.,'Subscription')]    40    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Guarantee##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=existingproductsTitle    40    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Payments#
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=divPayments    40    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##User Profile##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //select[@name='language']
    Wait And Click Element    //select[@name='language']//option[contains(text(),'English')]
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Summary##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait for element    //div[@class='wizard-body ng-scope']
    Wait And Click Element    //div[@class='wizard-body ng-scope']//select[contains(@ng-model,'selectReasons')]
    Wait And Click Element    //div[@class='wizard-body ng-scope']//option[contains(text(),'Join account')]
    sleep    5s
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    Unselect Frame
    #Submit Button#
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    id=stageAdvanceActionContainer
    Sleep    16s
    Confirm Action
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=header_jwl_status    40    1
    Sleep    5s
    ${a}    Get Text    //div[@id='header_jwl_status']/div[1]/span
    Run Keyword If    '${a}' == \ 'Submitted'    log    Order Submitted successfully
    Unselect Frame
    wait for element    id=crmRibbonManager    60    2
    wait for element    //span[@id='TabSearch']//input[@id='search']
    send text to element    //span[@id='TabSearch']//input[@id='search']    ${msisdn}
    Wait And Click Element    //span[@id='TabSearch']//img[@id='findCriteriaImg']
    wait for element    id=contentIFrame0    40    2
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    //div[@id='contentResult']    40    2
    Wait And Click Element    //div[@id='contentResult']//div[starts-with(@id,'Record_')]    40    2
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select Frame    id=contentIFrame1
    Unselect Frame
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']    60    2
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    //div[@id='etel_individualcustomerid']//span[@otypename='contact']
    ${n}    Get Text    //div[@id='etel_individualcustomerid']//span[@otypename='contact']
    log    ${n}
    Pass Execution If    '${name}' == '${n}'    Change ownership successfull
    Unselect Frame

ReactivateTemporaryDeactivatedSubscription
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    select Iframe    id=WebResource_processapp
    Sleep    25s
    wait for element    //table[@class='formSectionTable']//textarea[@name='notesTextArea']
    send text to element    //table[@class='formSectionTable']//textarea[@name='notesTextArea']    testing123
    Wait And Click Element    id=stageAdvanceActionContainer
    Sleep    20s
    Confirm Action
    sleep    40s
    Unselect Frame
    #verification of order submitted successfully#
    sleep    10s
    ${status}    Get Text    //div[@id='header_jwl_status']/div/span
    Run Keyword If    '${status}'=='Submitted'    Log    Order submitted successfully
    Sleep    20s
    #verification of subscription status change to Active#
    GoToIndividualCustomersList
    Go To Spesific Individual Customer    CUST0000000601
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    sleep    20s
    Unselect Frame
    Select IFrame    id=contentIFrame1
    sleep    20s
    wait and click element    //h2[@id='subscriptions_header_h2']
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    ##### For loop to evaluate given subscription is activated ###
    : FOR    ${INDEX}    IN RANGE    1    10
    \    Log    ${INDEX}
    \    ${a}    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]/div
    \    ${b}    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[3]/nobr/span
    \    Run Keyword If    '${a}' \ \ == \ \ 'CONTR0000001554' \ and \ \ '${b}' \ \ == \ \ 'Active'    Exit For Loop
    Log    Subscription Reactivated
    Unselect Frame

ChangeOwnership-CustomerRequest
    [Arguments]    ${National_Id}    ${Cust_Name}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //select[@ng-change='changeCustomerType()']    60    2
    Wait And Click Element    //select[@ng-change='changeCustomerType()']//option[contains(text(),'Individual')]
    Wait And Click Element    id=selectSearchTypesId
    Wait And Click Element    //select[@id='selectSearchTypesId']//option[contains(text(),'National Id')]
    Wait And Click Element    id=param1Id
    send text to element    id=param1Id    ${National_Id}
    Wait And Click Element    id=btn_searchCustomer
    sleep    10s
    Wait And Click Element    //div[@id='existingOfferingsGridBody']//input[@type='checkbox']
    Sleep    10s
    Wait Until Page Contains Element    //div[@id='processControlCollapsibleArea']//span[contains(text(),'${Cust_Name}')]
    ${msisdn}    Get Text    //div[@id='processStepsContainer']/div[1]//div[@class='process_accountNumber']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Log    ${msisdn}
    ${name}    Get Text    //div[@id='processStepsContainer']/div[2]//div[@class='process_accountNumber']//div[@class='processStepValueText processStepLookupValueText']
    Log    ${name}
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Open Invoices##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    //div[@class='wizard-body ng-scope']//p[starts-with(.,'Subscription')]    40    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Guarantee##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=existingproductsTitle    40    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Payments#
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=divPayments    40    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##User Profile##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //select[@name='language']
    Wait And Click Element    //select[@name='language']//option[contains(text(),'English')]
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Summary##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait for element    //div[@class='wizard-body ng-scope']
    Wait And Click Element    //div[@class='wizard-body ng-scope']//select[contains(@ng-model,'selectReasons')]
    Wait And Click Element    //div[@class='wizard-body ng-scope']//option[contains(text(),'Customer request')]
    sleep    5s
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    Unselect Frame
    #Submit Button#
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    id=stageAdvanceActionContainer
    Sleep    20s
    Confirm Action
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Sleep    5s
    wait for element    id=header_jwl_status    40    1
    ${a}    Get Text    //div[@id='header_jwl_status']/div[1]/span
    Run Keyword If    '${a}' == \ 'Submitted'    log    Order Submitted successfully
    Run Keyword If    '${a}' == 'Closed'    Log    order submitted successfully
    Unselect Frame
    Comment    sleep    30s
    wait for element    id=crmRibbonManager    60    2
    wait for element    //span[@id='TabSearch']//input[@id='search']
    send text to element    //span[@id='TabSearch']//input[@id='search']    ${msisdn}
    Wait And Click Element    //span[@id='TabSearch']//img[@id='findCriteriaImg']
    wait for element    id=crmRibbonManager    40    2
    wait for element    id=contentIFrame0    40    2
    Select IFrame    id=contentIFrame0
    Wait And Click Element    //div[@id='contentResult']//div[starts-with(@id,'Record_')]/div    40    2
    Unselect Frame
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']    60    2
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    wait for element    //div[@id='etel_individualcustomerid']//span[@otypename='contact']
    ${n}    Get Text    //div[@id='etel_individualcustomerid']//span[@otypename='contact']
    log    ${n}
    Pass Execution If    '${name}' == '${n}'    Change ownership successfull
    Unselect Frame

GetContractID
    [Arguments]    ${CUSTOMER_ID}    ${SIM}
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=processStepsContainer
    wait for element    id=processStep_corporateCustomer
    Double Click Element    //div[@id='processStep_corporateCustomer']//div[@class='processStepValue']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=subscriptions_header_h2
    click on element    id=subscriptions_header_h2
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to get contract id for customer ###
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${SIMFromWeb}=    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[6]
    \    log    ${SIMFromWeb}
    \    ${Contract_ID}    Run Keyword If    '${SIM}'=='${SIMFromWeb}'    get text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]
    \    log    ${Contract_ID}
    \    Exit For Loop If    '${SIM}'=='${SIMFromWeb}'
    ##### For loop ends ####
    Unselect Frame
    Write Excel    ${FilePath}/InputData.xls    1    1    6    ${Contract_ID}

VerifyOCCSubmit
    sleep    40s
    ${OCCSubSumitmessage}    Confirm Action
    log    ${OCCSubSumitmessage}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//div[@id='header_jwl_status']    40s
    ${StatusReasonOCC}    Get Text    xpath=//div[@id="header_jwl_status"]//div//span
    ${match}    Set Variable    ${StatusReasonOCC}
    ${value}    Set Variable    Submitted
    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${match}    ${value}
    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    log    ${RETURNVALUE}
    Run Keyword If    '${RETURNVALUE}'=='True'    Log    Apply OCC correction is succesfull :-    ${StatusReasonOCC}
    ...    ELSE    Log    Apply OCC correction is not succesfull ${StatusReasonOCC}

VerifySubmitBIOrderAndGetConID
    [Arguments]    ${CUSTOMER_ID}    ${SIM}
    Sleep    40s
    ${NewSubSumitmessage}    Confirm Action
    log    ${NewSubSumitmessage}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//div[@id='header_jwl_status']    40s
    ${StatusReasonNewSub}    Get Text    xpath=//div[@id="header_jwl_status"]//div//span
    ${match}    Set Variable    ${StatusReasonNewSub}
    ${value}    Set Variable    Submitted
    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${match}    ${value}
    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    log    ${RETURNVALUE}
    Run Keyword If    '${RETURNVALUE}'=='True'    Log    New Subscription \ sumbitted status is ${StatusReasonNewSub}
    ...    ELSE    Log    New Subscription \ sumbitted status is ${StatusReasonNewSub}
    Run Keyword If    '${RETURNVALUE}'=='True'    GetContractID    ${CUSTOMER_ID}    ${SIM}
    ...    ELSE    log    Order is not sumbitted succesfully. The error message is \ :- ${NewSubSumitmessage}

VerifyBIOrderSubmit
    [Arguments]    ${Successmsg}    ${Failuremsg}
    Sleep    40s
    ${NewSubSumitmessage}    Confirm Action
    log    ${NewSubSumitmessage}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//div[@id='header_jwl_status']    40s
    ${StatusReasonNewSub}    Get Text    xpath=//div[@id="header_jwl_status"]//div//span
    ${match}    Set Variable    ${StatusReasonNewSub}
    ${value}    Set Variable    Submitted
    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${match}    ${value}
    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    log    ${RETURNVALUE}
    ${Displaymsginlogpass}    Catenate    ${Successmsg}    ${StatusReasonNewSub}
    ${Displaymsginlogfail}    Catenate    ${Failuremsg}    ${StatusReasonNewSub}
    Run Keyword If    '${RETURNVALUE}'=='True'    Log    ${Displaymsginlogpass}
    ...    ELSE    Log    ${Displaymsginlogfail}
    [Return]    ${NewSubSumitmessage}

VerifyTemDeaSubStatus
    [Arguments]    ${CUSTOMER_ID}    ${Subscription_Id}
    Wait And Click Element    id=HomeTabLink
    Wait And Click Element    id=CS
    Wait And Click Element    id=nav_contacts
    Comment    GoToIndividualCustomersList
    OpenIndCustomerWithCustID    ${CUSTOMER_ID}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=subscriptions_header_h2
    click on element    id=subscriptions_header_h2
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to get contract id for customer ###
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${External_id}=    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]
    \    log    ${External_id}
    \    ${T1}    Convert To String    ${External_id}
    \    ${T2}    Convert To String    ${Subscription_Id}
    \    ${a}    Split String    ${T1}    CONTR
    \    ${b}    Split String    ${T2}    CONTR
    \    ${c}    Convert To String    ${b[1]}
    \    log    ${c}
    \    ${d}    Convert To String    ${a[1]}
    \    log    ${d}
    \    ${StatusFromWeb}    Run Keyword If    '${c}'=='${d}'    get text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[3]
    \    log    ${StatusFromWeb}
    \    Exit For Loop If    '${c}'=='${d}'
    \    Run Keyword If    '${StatusFromWeb}'=='Suspend'    log    Contract ${Subscription_Id} is temporary deactivated sucessfully
    \    ...    ELSE    Contract is not temporarily deactivated sucessfylly
    ##### For loop ends ####
    Unselect Frame

SubmitAndVerifyTempDeaOrder
    [Arguments]    ${CUSTOMER_ID}    ${Subscription_Id}
    wait for element    id=contentIFrame0
    Select Frame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select Frame    id=contentIFrame1
    wait for element    //div[@id='stageAdvanceActionContainer']
    click on element    //div[@id='stageAdvanceActionContainer']
    Unselect Frame
    wait for element    //li[@id='jwl_bi_temporarydeactivation|NoRelationship|Form|jwl.jwl_bi_temporarydeactivation.Submit.Button']
    sleep    2s
    click on element    //li[@id='jwl_bi_temporarydeactivation|NoRelationship|Form|jwl.jwl_bi_temporarydeactivation.Submit.Button']
    Sleep    30s
    ${NewSubSumitmessage}    Confirm Action
    log    ${NewSubSumitmessage}
    log    The message after order submit is :- ${NewSubSumitmessage}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    wait for element    id=header_statuscode    40s
    ${StatusReasonNewSub}    Get Text    xpath=//div[@id='header_statuscode']//div//span
    log    ${StatusReasonNewSub}
    ${match}    Set Variable    ${StatusReasonNewSub}
    ${value}    Set Variable    Submitted
    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${match}    ${value}
    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    log    ${RETURNVALUE}
    Unselect Frame
    Run Keyword If    '${RETURNVALUE}'=='True'    VerifyTemDeaSubStatus    ${CUSTOMER_ID}    ${Subscription_Id}
    ...    ELSE    log    Order is not sumbitted succesfully. The error message is \ :- ${NewSubSumitmessage}

VerifyUpdateAcc
    wait for element    id=contentIFrame1
    Select Frame    id=contentIFrame1
    wait for element    id=containerLoadingProgress
    wait for element    id=processControlScrollPane
    wait for element    id=processSteps_3
    wait for element    //div[@id='header_process_jwl_subscriptionid3']
    Focus    //div[@id='header_process_jwl_subscriptionid3']
    Double Click Element    //div[@id='header_process_jwl_subscriptionid3']
    Unselect Frame
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=subscriptionprofiles_header_h2
    click on element    id=subscriptionprofiles_header_h2
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//a[starts-with(@id,'gridBodyTable_primaryField')]
    click on element    xpath=//a[starts-with(@id,'gridBodyTable_primaryField')]
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    //div[@id='userprofilegrid_divDataArea']
    wait for element    //table[@id='gridBodyTable']
    ${Email_Text}    Get Text    //table[@id='gridBodyTable']//tbody//tr//td[5]
    log    ${Email_Text}
    Run Keyword If    '${UP_Email}'=='${Email_Text}'    log    PASS
    ...    ELSE    log    FAIL
    Unselect Frame

GetInvoiceVerification
    [Arguments]    ${INDEX}    ${Amount_for_verification}
    ${Open_Amount_Invoice}    Get Text    xpath=//div[@id='grid']//table//tbody//tr[${INDEX}]//td[5]
    log    ${Open_Amount_Invoice}
    Comment    ${Document_Amount}    Get Text    xpath=//div[@id='grid']//table//tbody//tr[${INDEX}]//td[4]
    Run Keyword If    '${Amount_for_verification}'=='${Open_Amount_Invoice}'    log    Invoice adjustment done
    ...    ELSE    log    Invoice adjustment is not correctly performed
    Comment    ${new_open_amt}    Evaluate    ${Open_Amount_Invoice}-1
    Comment    Run Keyword If    '${new_open_amt}'=='${Open_Amount_Invoice}'    log    Invoice adjustment done

InvoiceAdjustmentAndVerify
    [Arguments]    ${MSISDN}    ${Ad_Type}    ${Ad_Reason}    ${Ad_Amount}    ${Customer_Name}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_invoicegrid
    wait for element    //div[@id='grid']
    wait for element    //input[@id='searchSubs']
    click on element    //input[@id='searchSubs']
    send text to element    //input[@id='searchSubs']    ${MSISDN}
    #Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Click Element    //button[@id='searchbtn']
    #### Get count of the webtable ###
    sleep    2s
    Comment    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='grid']//table//tbody//tr//td[6]
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to evaluate the correct movie name ###
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${Open_Amount}=    Get Text    xpath=//div[@id='grid']//table//tbody//tr[${INDEX} ]//td[6]
    \    log    ${Open_Amount}
    \    Run Keyword If    '${Open_Amount}'>0    Select Checkbox    //div[@id='grid']//table//tbody//tr//td[1]//input
    \    sleep    1s
    \    Exit For Loop If    '${Open_Amount}'>0
    wait for element    //button[@id='applytop']
    Double Click Element    //button[@id='applytop']
    sleep    2s
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=jwl_adjustmenttype
    click on element    id=jwl_adjustmenttype
    click on element    //div[@id='jwl_adjustmenttype']//select[@id='jwl_adjustmenttype_i']//option[@title='${Ad_Type}']
    wait for element    id=jwl_adjustmentreason
    click on element    id=jwl_adjustmentreason
    click on element    //div[@id='jwl_adjustmentreason']//select[@id='jwl_adjustmentreason_i']//option[@title='${Ad_Reason}']
    wait for element    id=jwl_amount
    click on element    id=jwl_amount
    send text to element    //input[@id='jwl_amount_i']    ${Ad_Amount}
    ${Amount_for_verification}    Evaluate    ${Open_Amount}-1
    Unselect Frame
    wait for element    //li[@id='jwl_bi_invoiceadjustment|NoRelationship|Form|jwl.jwl_bi_invoiceadjustment.Submit.Button']
    click on element    //li[@id='jwl_bi_invoiceadjustment|NoRelationship|Form|jwl.jwl_bi_invoiceadjustment.Submit.Button']
    sleep    30s
    ${BISubmitMsg}    Confirm Action
    Run Keyword If    '${BISubmitMsg}'=='BI Invoice Adjustment Submitted Successfully.'    VerifyInvoiceAdjustment    ${Customer_Name}    ${MSISDN}    ${Amount_for_verification}    ESLE
    ...    log    BI is got error. Error details is :- ${BISubmitMsg}
    #sleep    2m
    #Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    #wait for element    id=contentIFrame0
    #Select IFrame    id=contentIFrame0
    #Comment    FrameSelect
    #wait for element    //div[@id='header_jwl_individualcustomerid']//label[@id='Individual Customer']
    #Double Click Element    //div[@id='header_jwl_individualcustomerid']//div//span[contains(text(),'${Customer_Name}')]
    #Unselect Frame
    #Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    #Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    #wait for element    id=contentIFrame0
    #Select IFrame    id=contentIFrame0
    #Comment    Unselect Frame
    #Comment    wait for element    id=contentIFrame1
    #Comment    Select IFrame    id=contentIFrame1
    #Comment    FrameSelect
    #sleep    5s
    #Run Keyword And Ignore Error    click element    id=tab_13_header_h2
    #sleep    2s
    #wait for element    //h2[@id='tab_10_header_h2']
    #click on element    //h2[@id='tab_10_header_h2']
    #sleep    10s
    #Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    #Select IFrame    id=WebResource_individual_invoiceGrid
    #wait for element    //div[@id='grid']
    #${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='grid']//table//tbody//tr//td[5]
    #log    ${functionBlockCount}
    #${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to evaluate the correct movie name ###
    #: FOR    ${INDEX}    IN RANGE    1    ${count}
    #\    ${Invoice_MSISDN}    Get Text    //div[@id='grid']//table//tbody//tr[${INDEX}]//td[2]
    #\    Run Keyword If    '${Invoice_MSISDN}'=='${MSISDN}'    GetInvoiceVerification    ${INDEX}    ${Amount_for_verification}
    #\    Exit For Loop If    '${Invoice_MSISDN}'=='${MSISDN}'
    #[Return]    ${Amount_for_verification}

VerifyInvoiceAdjustment
    [Arguments]    ${Customer_Name}    ${MSISDN}    ${Amount_for_verification}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    FrameSelect
    wait for element    //div[@id='header_jwl_individualcustomerid']//label[@id='Individual Customer']
    Double Click Element    //div[@id='header_jwl_individualcustomerid']//div//span[contains(text(),'${Customer_Name}')]
    Unselect Frame
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    Comment    FrameSelect
    sleep    5s
    Run Keyword And Ignore Error    click element    id=tab_13_header_h2
    sleep    2s
    wait for element    //h2[@id='tab_10_header_h2']
    click on element    //h2[@id='tab_10_header_h2']
    sleep    10s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Select IFrame    id=WebResource_individual_invoiceGrid
    wait for element    //div[@id='grid']
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='grid']//table//tbody//tr//td[5]
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to evaluate the correct movie name ###
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${Invoice_MSISDN}    Get Text    //div[@id='grid']//table//tbody//tr[${INDEX}]//td[2]
    \    Run Keyword If    '${Invoice_MSISDN}'=='${MSISDN}'    GetInvoiceVerification    ${INDEX}    ${Amount_for_verification}
    \    Exit For Loop If    '${Invoice_MSISDN}'=='${MSISDN}'

FreezeSubscription
    [Arguments]    ${Subscription_Id}    ${Notes}
    wait for element    id=etel_ordercapture|NoRelationship|Form|Mscrm.Form.etel_ordercapture.Save    60    2
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp    60    2
    select Iframe    id=WebResource_processapp
    wait for element    //textarea[@name='notesTextArea']    60    2
    send text to element    //textarea[@name='notesTextArea']    ${Notes}
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Installmment#
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=existingOfferingsGridHeader    60    2
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Documents#
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=dowloadDocumentsUrlGridHeader    60    2
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Summary#
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    //div[@title="Payment method"]    60    2
    Comment    Wait And Click Element    id=stageAdvanceActionContainer
    Comment    Sleep    15s
    ${Submitmessage}    Confirm Action
    Log    Message after submission is : ${Submitmessage}
    Sleep    5s
    wait for element    id=header_jwl_status
    ${status}    Get Text    //div[@id='header_jwl_status']/div/span
    Run Keyword If    '${status}' == 'Submitted'    VerifyFreezeSubscriptionStatus    ${Subscription_Id}
    ...    ELSE    log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}
    Run Keyword If    '${status}' == 'Closed'    VerifyFreezeSubscriptionStatus    ${Subscription_Id}
    ...    ELSE    log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}

VerifyFreezeSubscriptionStatus
    [Arguments]    ${Subscription_Id}
    Wait And Click Element    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_corporateCustomer']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Unselect Frame
    wait for element    id=contact|NoRelationship|Form|jwl.contact.Actions.Button    60    2
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    sleep    10s
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait and click element    //h2[@id='subscriptions_header_h2']    40    2
    Unselect Frame
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']    40    2
    #For Loop will only open provided subscription if it is active##
    : FOR    ${INDEX}    IN RANGE    1    10
    \    Log    ${INDEX}
    \    ${a}    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]/div
    \    ${b}    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[3]/nobr/span
    \    Run Keyword If    '${a}' \ \ == \ \ '${Subscription_Id}' \ and \ \ '${b}' \ \ == \ \ 'Suspended'    Exit For Loop    Log    Freeze Subscription completed successfully
    \    ...    ELSE    log    Freeze Subscription not completed successfully

VerifyOfferChange
    [Arguments]    ${offer}
    Wait And Click Element    //div[@id='processStepsContainer']/div[1]//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Unselect Frame
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']    60    2
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    wait for element    //td[@id='etel_subscriptionofferingid_d']//span[@otypename='product']
    ${o}    Get Text    //td[@id='etel_subscriptionofferingid_d']//span[@otypename='product']
    Log    ${o}
    Should Be Equal    ${o}    ${offer}
    Unselect Frame

InvoicePaymentAndVerify
    [Arguments]    ${MSISDN}    ${Payment_Method}    ${Amount}    ${Customer_Name}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_InvoicePayment
    wait for element    //div[@id='grid']
    wait for element    //input[@id='searchSubs']
    click on element    //input[@id='searchSubs']
    send text to element    //input[@id='searchSubs']    ${MSISDN}
    #Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Click Element    //button[@id='searchbtn']
    #### Get count of the webtable ###
    sleep    2s
    Comment    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='grid']//table//tbody//tr//td[6]
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to evaluate the correct movie name ###
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${Open_Amount}=    Get Text    xpath=//div[@id='grid']//table//tbody//tr[${INDEX} ]//td[6]
    \    log    ${Open_Amount}
    \    Run Keyword If    '${Open_Amount}'>0    Select Checkbox    //div[@id='grid']//table//tbody//tr//td[1]//input
    \    sleep    1s
    \    Exit For Loop If    '${Open_Amount}'>0
    wait for element    //button[@id='applytop']
    Double Click Element    //button[@id='applytop']
    sleep    2s
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=jwl_paymentmethod
    click on element    id=jwl_paymentmethod
    click on element    //select[@id='jwl_paymentmethod_i']//option[@title='${Payment_Method}']
    wait for element    id=jwl_freeamount
    click on element    id=jwl_freeamount
    #click on element    //div[@id='jwl_adjustmentreason']//select[@id='jwl_adjustmentreason_i']//option[@title='${Ad_Reason}']
    #wait for element    id=jwl_amount
    #click on element    id=jwl_amount
    send text to element    //input[@id='jwl_freeamount_i']    ${Amount}
    wait for element    id=jwl_collectorid
    click on element    id=jwl_collectorid_i
    click on element    //li[@id='item0']//a[@class='ms-crm-IL-MenuItem-Anchor ms-crm-IL-MenuItem-Anchor-Rest']
    ${Amount_for_verification}    Evaluate    ${Open_Amount}-${Amount}
    Unselect Frame
    wait for element    //li[@id='jwl_bi_payinvoice|NoRelationship|Form|jwl.jwl_bi_payinvoice.Submit.Button']
    click on element    //li[@id='jwl_bi_payinvoice|NoRelationship|Form|jwl.jwl_bi_payinvoice.Submit.Button']
    sleep    30s
    ${BISubmitMsg}    Confirm Action
    Run Keyword If    '${BISubmitMsg}'=='BI Invoice Payment Submitted Successfully.'    VerifyInvoicePayment    ${Customer_Name}    ${MSISDN}    ${Amount_for_verification}    ESLE
    ...    log    BI is got error. Error details is :- ${BISubmitMsg}

VerifyInvoicePayment
    [Arguments]    ${Customer_Name}    ${MSISDN}    ${Amount_for_verification}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    FrameSelect
    wait for element    //div[@id='header_jwl_individualcustomerid']//label[@id='Individual Customer']
    Double Click Element    //div[@id='header_jwl_individualcustomerid']//div//span[contains(text(),'${Customer_Name}')]
    Unselect Frame
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    Comment    FrameSelect
    sleep    5s
    Run Keyword And Ignore Error    click element    id=tab_13_header_h2
    sleep    2s
    wait for element    //h2[@id='tab_10_header_h2']
    click on element    //h2[@id='tab_10_header_h2']
    sleep    10s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Select IFrame    id=WebResource_individual_invoiceGrid
    wait for element    //div[@id='grid']
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='grid']//table//tbody//tr//td[5]
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to evaluate the correct movie name ###
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${Invoice_MSISDN}    Get Text    //div[@id='grid']//table//tbody//tr[${INDEX}]//td[2]
    \    Run Keyword If    '${Invoice_MSISDN}'=='${MSISDN}'    GetInvoicePaymentVerification    ${INDEX}    ${Amount_for_verification}
    \    Exit For Loop If    '${Invoice_MSISDN}'=='${MSISDN}'

GetInvoicePaymentVerification
    [Arguments]    ${INDEX}    ${Amount_for_verification}
    ${Open_Amount_Invoice}    Get Text    xpath=//div[@id='grid']//table//tbody//tr[${INDEX}]//td[5]
    log    ${Open_Amount_Invoice}
    Comment    ${Document_Amount}    Get Text    xpath=//div[@id='grid']//table//tbody//tr[${INDEX}]//td[4]
    Run Keyword If    '${Amount_for_verification}'=='${Open_Amount_Invoice}'    log    Invoice payment done
    ...    ELSE    log    Invoice payment is not correctly performed
    Comment    ${new_open_amt}    Evaluate    ${Open_Amount_Invoice}-1
    Comment    Run Keyword If    '${new_open_amt}'=='${Open_Amount_Invoice}'

BalanceRefillAndVerify
    [Arguments]    ${Refill_Amount}    ${Refill_Profile}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    FrameSelect
    wait for element    //div[@id='jwl_refillamount']
    click on element    //div[@id='jwl_refillamount']
    Comment    click on element    //input[@id='jwl_refillamount_i']
    send text to element    //input[@id='jwl_refillamount_i']    ${Refill_Amount}
    wait for element    //div[@id='etel_refillprofile1']
    click on element    //div[@id='etel_refillprofile1']
    click on element    //div[@id='etel_refillprofile1']//select[@id='etel_refillprofile1_i']//option[@title='${Refill_Profile}']
    ${Reason}    Get Text    //div[@id='etel_refillreason2']
    log    ${Reason}
    Run Keyword If    '${Reason}'=='Refill'    log    Refill reason is correct
    ...    ELSE    log    Refill reason is incorrect. The available reason is :- ${Reason}
    Unselect Frame
    wait for element    //li[@id='etel_birefill|NoRelationship|Form|etel.etel_birefill.Submit.Command']
    click on element    //li[@id='etel_birefill|NoRelationship|Form|etel.etel_birefill.Submit.Command']
    sleep    30s
    ${BISubMsg}    Confirm Action
    log    ${BISubMsg}
    ${match}    Set Variable    ${BISubMsg}
    ${value}    Set Variable    Submitted
    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${match}    ${value}
    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    log    ${RETURNVALUE}
    Run Keyword If    '${RETURNVALUE}'=='True'    Log    Balance Refill BI status is ${BISubMsg}
    ...    ELSE    Log    Balance Refill BI status is ${BISubMsg}

EnterActivationCode
    [Arguments]    ${ActSerialNum}
    wait for element    //div[@id='jwl_activationcode']
    click on element    //div[@id='jwl_activationcode']
    send text to element    //input[@id='jwl_activationcode_i']    ${ActSerialNum}

EnterSerialNumber
    [Arguments]    ${ActSerialNum}
    wait for element    //div[@id='jwl_serialno']
    click on element    //div[@id='jwl_serialno']
    send text to element    //input[@id='jwl_serialno_i']    ${ActSerialNum}

VoucherRefillAndVerify
    [Arguments]    ${Proceed_Code}    ${ActiCode_Serial_num}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    FrameSelect
    wait for element    //div[@id='jwl_proceedwith']
    click on element    //div[@id='jwl_proceedwith']
    wait for element    //div[@id='jwl_proceedwith']//select[@id='jwl_proceedwith_i']//option[@title='${Proceed_Code}']
    click on element    //div[@id='jwl_proceedwith']//select[@id='jwl_proceedwith_i']//option[@title='${Proceed_Code}']
    run keyword if    '${Proceed_Code}'=='Activation Code'    EnterActivationCode    ${ActiCode_Serial_num}
    ...    ELSE    EnterSerialNumber    ${ActiCode_Serial_num}
    wait for element    id=WebResource_voucherrefillhandler
    Select IFrame    id=WebResource_voucherrefillhandler
    wait for element    //button
    click on element    //button
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    //li[@id='etel_birefill|NoRelationship|Form|etel.etel_birefill.Submit.Command']
    click on element    //li[@id='etel_birefill|NoRelationship|Form|etel.etel_birefill.Submit.Command']
    sleep    30s
    ${BISubMsg}    Confirm Action
    log    ${BISubMsg}
    sleep    5s
    ${validationmsg}    Set Variable    ${BISubMsg}
    ${value}    Set Variable    Submitted
    Comment    ${value1}    Set Variable    Closed
    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${validationmsg}    ${value}
    Comment    ${match1}    ${value1}    Run Keyword And Ignore Error    Should Contain    ${validationmsg}    ${value1}
    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    Comment    ${RETURNVALUE1}    Set Variable If    '${match1}' == 'PASS'    ${True}    ${False}
    log    ${RETURNVALUE}
    Comment    Run Keyword If    '${RETURNVALUE1}'=='True'    Log    Voucher Refill BI status is ${BISubMsg}
    ...    ELSE    Log    Voucher Refill BI status is ${BISubMsg}
    Run Keyword If    '${RETURNVALUE}'=='True'    Log    Voucher Refill BI status is ${BISubMsg}
    ...    ELSE    Log    Voucher Refill BI status is ${BISubMsg}

SKFPSubmitAndVerify
    [Arguments]    ${SKFP_num}    ${User_Profile_Lang}    ${Summary_Lan}
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='left']//input
    click on element    //div[@id='left']//input
    send text to element    //div[@id='left']//input    ${SKFP_num}
    wait for element    //button[@id='btn_SearchSKFP']
    click on element    //button[@id='btn_SearchSKFP']
    ${MSISDN}    Get Text    //div[@id='left']//table//tbody//tr//td//div[@class='formFieldValue ng-binding']
    log    ${MSISDN}
    wait for element    //button[@id='btn_saveResultSkfp']
    click on element    //button[@id='btn_saveResultSkfp']
    wait for element    id=stageAdvanceActionContainer
    click on element    id=stageAdvanceActionContainer
    Unselect Frame
    NewSubUserProfilePage    ${User_Profile_Lang}
    ###Eligibility
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    select Iframe    id=WebResource_processapp
    #wait for element    id=btnConfigure    60    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ###Payment
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    select Iframe    id=WebResource_processapp
    #wait for element    id=btnConfigure    60    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ###Summary
    NewSubSummaryPage    ${Summary_Lan}
    Sleep    40s
    ${NewSubSumitmessage}    Confirm Action
    log    ${NewSubSumitmessage}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//div[@id='header_jwl_status']    40s
    ${StatusReasonNewSub}    Get Text    xpath=//div[@id="header_jwl_status"]//div//span
    ${match}    Set Variable    ${StatusReasonNewSub}
    ${value}    Set Variable    Submitted
    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${match}    ${value}
    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    log    ${RETURNVALUE}
    Run Keyword If    '${RETURNVALUE}'=='True'    Log    New Subscription \ sumbitted status is ${StatusReasonNewSub}
    ...    ELSE    Log    New Subscription \ sumbitted status is ${StatusReasonNewSub}
    Run Keyword If    '${RETURNVALUE}'=='True'    VerifySKFP    ${MSISDN}
    ...    ELSE    log    SKFP not submitted succesfully. Order status is :- {StatusReasonNewSub}

VerifySKFP
    [Arguments]    ${MSISDN}
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=processStepsContainer
    wait for element    id=processStep_corporateCustomer
    Double Click Element    //div[@id='processStep_corporateCustomer']//div[@class='processStepValue']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=subscriptions_header_h2
    click on element    id=subscriptions_header_h2
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[2]
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to get contract id for customer ###
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${MSISDNFromWeb}=    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[2]
    \    log    ${MSISDNFromWeb}
    \    Exit For Loop If    '${MSISDNFromWeb}'=='${MSISDN}'
    ##### For loop ends ####
    Unselect Frame

CancelOrderAndVerify
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${Order_ID}    Get text    //div[@id='FormTitle']
    log    ${Order_ID}
    Unselect Frame
    wait for element    //li[@id='etel_ordercapture|NoRelationship|Form|jwl.etel_ordercapture.CancelOrder.Button']
    click on element    //li[@id='etel_ordercapture|NoRelationship|Form|jwl.etel_ordercapture.CancelOrder.Button']
    sleep    20s
    ${OrderSubMsg}    Confirm Action
    log    ${OrderSubMsg}
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=processStepsContainer
    wait for element    id=processStep_corporateCustomer
    Double Click Element    //div[@id='processStep_corporateCustomer']//div[@class='processStepValue']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //span[@id='TabNode_tab0Tab']
    click on element    //span[@id='TabNode_tab0Tab']
    wait for element    //a[@id='Node_nav_etel_contact_ordercapture_individualcustomerid']
    click on element    //a[@id='Node_nav_etel_contact_ordercapture_individualcustomerid']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    select IFrame    id=contentIFrame0
    Unselect Frame
    Select IFrame    id=contentIFrame1
    wait for element    id=area_etel_contact_ordercapture_individualcustomeridFrame
    select IFrame    id=area_etel_contact_ordercapture_individualcustomeridFrame
    wait for element    //a[@id='crmGrid_etel_contact_ordercapture_individualcustomerid_SavedNewQuerySelector']
    click on element    //a[@id='crmGrid_etel_contact_ordercapture_individualcustomerid_SavedNewQuerySelector']
    wait for element    //li[@id='{36A67C39-5AD0-E311-B005-005056AE11FF}']//span[contains(text(),'All Orders')]
    click on element    //li[@id='{36A67C39-5AD0-E311-B005-005056AE11FF}']//span[contains(text(),'All Orders')]
    wait for element    //table[@id='gridBodyTable']
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//table[@id='gridBodyTable']//tbody//tr//td[3]
    ${count}=    Evaluate    ${functionBlockCount}+1
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${OrderFromPage}    Get text    xpath=//table[@id='gridBodyTable']//tbody//tr[${INDEX}]//td[3]
    \    ${OrderStatus} =    Run Keyword If    '${OrderFromPage}'=='${Order_ID}'    get text    xpath=//table[@id='gridBodyTable']//tbody//tr[${INDEX}]//td[5]
    \    log    ${OrderStatus}
    \    Run Keyword If    '${OrderStatus}'=='Cancel'    log    cancel order BI is validated
    \    ...    ELSE    log    Cancel order BI is not validated. Order status is :- ${OrderStatus}
    \    Exit For Loop If    '${OrderFromPage}'=='${Order_ID}'

ReviewFinHisAndVerify
    [Arguments]    ${From_date}    ${To_date}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    wait for element    id=contentIFrame0
    Comment    Select IFrame    id=contentIFrame0
    FrameSelect
    Run Keyword And Ignore Error    wait and click element    //h2[@id='tab_16_header_h2']
    Run Keyword And Ignore Error    Wait And Click Element    id=tab_13_header_h2
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    Unselect Frame
    Comment    Select IFrame    id=contentIFrame0
    wait and click element    id=review_financial_history_header_h2
    wait for element    id=WebResource_review_financial_history
    Select IFrame    id=WebResource_review_financial_history
    wait for element    id=FromDate
    click on element    id=FromDate
    send text to element    //input[@id='FromDate']    ${From_date}
    wait for element    id=EndDate
    click on element    id=EndDate
    send text to element    //input[@id='EndDate']    ${To_date}
    wait for element    id=btnSearch
    click on element    id=btnSearch
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${Present}    Run Keyword And Return Status    Page Should Contain Element    //table[@id='financialHistGrid']//tbody//tr
    log    ${Present}
    Run Keyword If    '${Present}'=='True'    Log    Financial history is present for given dates
    ...    ELSE    Financial history is not present

VerifyAccHistory
    FrameSelect
    Select IFrame    id=WebResource_ah
    wait for element    //button[@id='search']
    click on element    //button[@id='search']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${Present}    Run Keyword And Return Status    Page Should Contain Element    //table[@id='accountHistoryGrid']//tbody//tr
    log    ${Present}
    Run Keyword If    '${Present}'=='True'    Log    Account History is present for selected subscription
    ...    ELSE    log    Account History is not present for selected subscription

CreateNewPostPaidSubscription
    [Arguments]    ${Offer}    ${MSISDN}    ${SIM}    ${User_profile_Language}    ${Summary_Billing_Lan}    ${Notes}
    ...    ${IMEI}
    #Products#
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=newCustomerSearchButtonGroup
    Wait And Click Element    xpath=//div[@id='newCustomerSearchButtonGroup']//button[contains(text(),'SEARCH')]
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //div[@class='dataGridWrapper']
    Wait And Click Element    xpath=//table//tbody//tr[@class='dataGridBodyRow ng-scope']//div[@class='dataGridCellContent']//span[contains(text(),'${Offer}')]
    wait and click element    xpath=//span[@class='cmdBarBtnSpan ng-binding'][@ng-click='!rootScopeData.currentStage.isActive || scopeData.planSelection.addBasket()']
    Unselect Frame
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=orderBasket
    ${alloffers}    Get Text    xpath=//div[@id='orderBasket']//div[@class='dataGridBodyScroll']//tr[@class='dataGridBodyRow ng-scope']//td[1]//span[@class='lowerCellSection ng-binding']
    log    ${alloffers}
    Run Keyword If    '${alloffers}'=='${Offer}'    Log    Correct offer is added in Basket
    ...    ELSE    log    Offer is not added in bascket. Offer in bascket is = ${alloffers}
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame
    #Configuration#
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    wait for element    xpath=//table//tbody//tr[@class='dataGridBodyRow ng-scope']//div[@class='dataGridCellContent']//span[contains(text(),'${Offer}')]    40    1
    Sleep    5s
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame
    #Resources#
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //div[@id='FormTitle']/h1[@class='ms-crm-TextAutoEllipsis']    40    1
    ${orderid}    Get Text    //div[@id='FormTitle']/h1[@class='ms-crm-TextAutoEllipsis']
    Log    ${orderid}
    select Iframe    id=WebResource_processapp
    wait for element    id=resourcesBlock    60    1
    wait for element    //input[@id='txtMSISDN']
    click on element    //input[@id='txtMSISDN']
    send text to element    //input[@id='txtMSISDN']    ${MSISDN}
    click on element    //input[@id='txtSIM']
    send text to element    //input[@id='txtSIM']    ${SIM}
    click on element    //input[@id='txtMSISDN']
    ###Reserve MSISDN###
    Wait And Click Element    //button[@id='btnSearchMSISDN']
    Unselect Frame
    sleep    30s
    @{Allwindows}    Get Window Titles
    ${Countofwindows}    Get Length    ${Allwindows}
    Comment    ${CountForLoop}    Evaluate    ${Countofwindows}+1
    : FOR    ${I}    IN RANGE    0    ${Countofwindows}
    \    ${title}    Get From List    ${Allwindows}    ${I}
    \    log    ${title}
    \    Run Keyword If    '${title}'=='undefined'    Select Window    ${Allwindows[${I}]}
    \    Run Keyword If    '${title}'!='undefined'    SelectTitleOfWindow    @{Allwindows}
    \    Exit For Loop
    Comment    Wait Until Keyword Succeeds    40    1    Select Window    @{Allwindows}[1]
    wait for element    //h5[contains(text(),'CRITERIA')]    40    2
    wait for element    //span[@aria-controls='msisdnCriteria_listbox']
    Click on element    //span[@aria-controls='msisdnCriteria_listbox']
    Sleep    1s
    wait for element    //ul[@id='msisdnCriteria_listbox']//li[contains(text(),'Equals')]
    click on element    //ul[@id='msisdnCriteria_listbox']//li[contains(text(),'Equals')]
    wait for element    id=msisdnValue
    click on element    id=msisdnValue
    send text to element    id=msisdnValue    ${MSISDN}
    Click on element    id=query
    Close Window
    Comment    Wait And Click Element    //div[@id='QueryGrid']//input[@type='checkbox']
    Comment    click on element    id=select
    Comment    sleep    20s
    Comment    Confirm Action
    Wait Until Keyword Succeeds    40    1    Select Window    title=Order Capture: ${orderid} - Microsoft Dynamics CRM
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Comment    click on element    //button[@id='btnAssignSIM']
    Comment    sleep    40s
    Comment    ${SIMAssignmessage}    Confirm Action
    Comment    log    ${SIMAssignmessage}
    Comment    Log To Console    ${SIMAssignmessage}
    Comment    Run Keyword If    '${SIMAssignmessage}'=='Sim has reserved successfully'    Log    SIM Reservered successfully
    ...    ELSE
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    ##Reserve MSISDN Ends##
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame
    #Add On Expiration#
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame
    #User Profile#
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='left']//select[@name='language']
    wait and click element    //div[@id='left']//select[@name='language']//option[contains(text(),'${User_profile_Language}')]
    Unselect Frame
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame
    #Handset with Installment#
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='createInstallment']/select    40    1
    Wait And Click Element    //div[@id='createInstallment']/select
    Wait And Click Element    //div[@id='createInstallment']//select//option[@label='Commitment and Installments 12Months']
    Wait And Click Element    id=btn_createInstallment    40    2
    Wait And Click Element    //div[@id='existingOfferingsGridBody']//tr[@class='dataGridBodyRow ng-scope']/td[1]//a[@class='ng-binding']
    unselect frame
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Sleep    5s
    Run Keyword And Continue On Failure    Confirm Action
    Sleep    5s
    wait for element    //div[@id='jwl_offeringproductid']
    click on element    //div[@id='jwl_offeringproductid']
    wait for element    //input[@id='jwl_offeringproductid_ledit']
    click on element    //input[@id='jwl_offeringproductid_ledit']
    wait for element    //img[@id='jwl_offeringproductid_i']
    click on element    //img[@id='jwl_offeringproductid_i']
    wait for element    //ul[@id='jwl_offeringproductid_i_IMenu']/li[1]
    click on element    //ul[@id='jwl_offeringproductid_i_IMenu']/li[1]
    sleep    2s
    Run Keyword And Continue On Failure    Confirm Action
    sleep    3s
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    //div[@id='jwl_handsettierid']
    click on element    //div[@id='jwl_handsettierid']
    wait for element    //input[@id='jwl_handsettierid_ledit']
    click on element    //input[@id='jwl_handsettierid_ledit']
    wait for element    //img[@id='jwl_handsettierid_i']
    click on element    //img[@id='jwl_handsettierid_i']
    wait for element    //ul[@id='jwl_handsettierid_i_IMenu']/li[1]
    click on element    //ul[@id='jwl_handsettierid_i_IMenu']/li[1]
    sleep    2s
    Wait And Click Element    id=jwl_product_imei
    wait for element    id=jwl_product_imei_i
    send text to element    id=jwl_product_imei_i    1234657898765435
    sleep    2s
    Run Keyword And Ignore Error    wait for element    //div[@id='jwl_installmenttypegroupid']
    Run Keyword And Ignore Error    click on element    //div[@id='jwl_installmenttypegroupid']
    Run Keyword And Ignore Error    wait for element    //input[@id='jwl_installmenttypegroupid_ledit']
    Run Keyword And Ignore Error    click on element    //input[@id='jwl_installmenttypegroupid_ledit']
    Run Keyword And Ignore Error    wait for element    //img[@id='jwl_installmenttypegroupid_i']
    Run Keyword And Ignore Error    click on element    //img[@id='jwl_installmenttypegroupid_i']
    Run Keyword And Ignore Error    wait for element    //ul[@id='jwl_installmenttypegroupid_i_IMenu']//li[1]
    Run Keyword And Ignore Error    click on element    //ul[@id='jwl_installmenttypegroupid_i_IMenu']//li[1]
    sleep    2s
    Run Keyword And Ignore Error    wait for element    //div[@id='jwl_installmenttypeid']
    Run Keyword And Ignore Error    click on element    //div[@id='jwl_installmenttypeid']
    Run Keyword And Ignore Error    wait for element    //input[@id='jwl_installmenttypeid_ledit']
    Run Keyword And Ignore Error    click on element    //input[@id='jwl_installmenttypeid_ledit']
    Run Keyword And Ignore Error    wait for element    //img[@id='jwl_installmenttypeid_i']
    Run Keyword And Ignore Error    click on element    //img[@id='jwl_installmenttypeid_i']
    Run Keyword And Ignore Error    wait for element    //ul[@id='jwl_installmenttypeid_i_IMenu']//li[1]
    Run Keyword And Ignore Error    click on element    //ul[@id='jwl_installmenttypeid_i_IMenu']//li[1]
    Unselect Frame
    Run Keyword And Continue On Failure    click on element    //span[contains(text(),'Validate IMEI')]
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Sleep    8s
    Run Keyword And Continue On Failure    Confirm Action
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    id=jwl_bi_installmentorder|NoRelationship|Form|Mscrm.Form.jwl_bi_installmentorder.SaveAndClose
    wait for element    //span[contains(text(),'Documents')]    60    2
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp    40    1
    select Iframe    id=WebResource_processapp
    wait for element    id=btn_createInstallment    40    2
    wait for element    //div[contains(text(),'Existing Installment Plan')]    40    1
    wait for element    //div[@id='existingOfferingsGridBody']    40    2
    sleep    10s
    ${Base}    Get Text    //div[@id='existingOfferingsGridBody']//table//tr//td[3]
    ${Discount}    Get Text    //div[@id='existingOfferingsGridBody']//table//tr//td[4]
    ${Total}    Get Text    //div[@id='existingOfferingsGridBody']//table//tr//td[5]
    ##check Installment##
    Comment    ${AfterDiscount}=    Evaluate    ${Base} - ${Discount}
    Comment    Run Keyword If    ${AfterDiscount}==${Total}    Log    Total Amount is calculated right
    ...    ELSE    Log    Total Amount is calculated wrong
    #Check Installment Ends#
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame
    #Guarantee#
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='downloadDocsTitle']//div[contains(text(),'GUARANTEES')]    40    1
    ${GuaranteeCheck}    Get Text    //button[@id='guaranteeButton']
    Log    ${GuaranteeCheck}
    Run Keyword If    '${GuaranteeCheck}' == 'Edit Guarantee'    GuaranteeCheck    ${Notes}    ${orderid}
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame
    #Financials and payments#
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=divPayments    40    1
    Wait And Click Element    id=btnConfigure
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=jwl_collectionmethod    60    1
    Unselect Frame
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Sleep    5s
    Wait And Click Element    id=jwl_payment|NoRelationship|Form|Mscrm.Form.jwl_payment.SaveAndClose    60    1
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame
    #Summary#
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    //table[@id='formField_noSubscriptions']    40    1
    Wait And Click Element    //table[@id='formField_noSubscriptions']/tbody/tr[2]/td[2]/div/select
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    5s
    Wait And Click Element    //table[@id='formField_noSubscriptions']/tbody/tr[2]/td[2]/div/select
    Wait And Click Element    //table[@id='formField_noSubscriptions']//option[contains(text(),'English')]
    ##Submit Check##
    Wait And Click Element    id=stageAdvanceActionContainer
    sleep    20s
    ${Submitmessage}    Confirm Action
    Log    Submit message is-${Submitmessage}
    Sleep    5s
    wait for element    id=header_jwl_status
    ${status}    Get Text    //div[@id='header_jwl_status']/div/span
    Run Keyword If    '${status}' == 'Submitted'    VerifyCreateNewPostPaidSubscription    ${MSISDN}
    ...    ELSE    Log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}
    Run Keyword If    '${status}' == 'Closed'    VerifyCreateNewPostPaidSubscription    ${MSISDN}
    ...    ELSE    Log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}
    ##Submit Check Ends##

GuaranteeCheck
    [Arguments]    ${Notes}    ${orderid}
    Wait And Click Element    //button[@id='guaranteeButton']    40    2
    sleep    5s
    Unselect Frame
    #@{AllGuaWindows}    Get Window Titles
    Comment    log many    @{AllGuaWindows}
    Comment    Run Keyword If    '@{AllGuaWindows}[1]'=='undefined'    Select Window    New
    ...    ELSE    Select Window    @{AllGuaWindows}[1]
    Comment    Wait Until Keyword Succeeds    120    1    Select Window    new
    Comment    Run Keyword And Continue On Failure    closeexploreCMR
    @{Allwindows}    Get Window Titles
    ${Countofwindows}    Get Length    ${Allwindows}
    Comment    ${CountForLoop}    Evaluate    ${Countofwindows}+1
    : FOR    ${I}    IN RANGE    0    ${Countofwindows}
    \    ${title}    Get From List    ${Allwindows}    ${I}
    \    log    ${title}
    \    Run Keyword If    '${title}'=='undefined'    Select Window    New
    \    ...    ELSE    SelectTitleOfWindow    @{Allwindows}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait And Click Element    id=jwl_guaranteecheck    40    1
    Wait And Click Element    id=jwl_guaranteetype
    Wait And Click Element    //select[@id='jwl_guaranteetype_i']//option[contains(text(),'Good / Medium Credit Score')]
    wait for element    id=jwl_services
    click on element    id=jwl_services
    wait for element    //select[@id='jwl_services_i']
    click on element    //select[@id='jwl_services_i']/option[2]
    Wait And Click Element    id=jwl_notes
    send text to element    id=jwl_notes_i    ${Notes}
    Wait And Click Element    id=jwl_regionid
    Wait And Click Element    id=jwl_regionid_ledit
    Wait And Click Element    //img[@id='jwl_regionid_i']
    wait for element    id=jwl_regionid_i_IMenu
    Wait And Click Element    //ul[@id='jwl_regionid_i_IMenu']/li[1]
    sleep    2s
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    //li[@id='jwl_guarantee|NoRelationship|Form|Mscrm.Form.jwl_guarantee.SaveAndClose']
    Run Keyword And Continue On Failure    click on element    //li[@id='jwl_guarantee|NoRelationship|Form|Mscrm.Form.jwl_guarantee.SaveAndClose']
    wait for element    //li[@id='jwl_guarantee|NoRelationship|Form|jwl.jwl_guarantee.Button1.Button']
    Run Keyword And Ignore Error    Click Element    //li[@id='jwl_guarantee|NoRelationship|Form|jwl.jwl_guarantee.Button1.Button']
    Wait Until Keyword Succeeds    40    1    Select Window    title=Order Capture: ${orderid} - Microsoft Dynamics CRM
    sleep    7s
    wait for element    id=contentIFrame0    40    1
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    select Iframe    id=WebResource_processapp
    sleep    10s
    ${checked}    Get Value    //div[@id='guaranteeGridBody']//tr[@class='dataGridBodyRow ng-scope']//td[2]/div/span
    Run Keyword If    '${checked}' == 'Yes'    Log    Guarantee has been successfully checked
    ...    ELSE    Log    Guarantee has not been successfully checked

VerifyCreateNewPostPaidSubscription
    [Arguments]    ${MSISDN}
    Wait And Click Element    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_corporateCustomer']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Unselect Frame
    wait for element    id=contact|NoRelationship|Form|jwl.contact.Actions.Button    60    2
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    sleep    10s
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait and click element    //h2[@id='subscriptions_header_h2']    40    2
    Unselect Frame
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']    40    2
    #For Loop will only open provided subscription if it is active##
    : FOR    ${INDEX}    IN RANGE    1    10
    \    Log    ${INDEX}
    \    ${a}    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[2]/div
    \    ${b}    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[3]/nobr/span
    \    Run Keyword If    '${a}' \ \ == \ \ '${MSISDN}' \ and \ \ '${b}' \ \ == \ \ 'Active'    Exit For Loop    Log    PostPaidSubscription completed successfully
    \    ...    ELSE    log    PostPaid Subscription not completed successfully

Submit FamilyPackage
    [Arguments]    ${GovtID}    ${FatherSubs_Id}    ${MotherSubs_Id}    ${ChildSubs_Id}
    #Father#
    wait for element    id=jwl_bi_familypackage|NoRelationship|Form|Mscrm.Form.jwl_bi_familypackage.Save    60    2
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_SearchIndividual    40    1
    Select IFrame    id=WebResource_SearchIndividual
    wait for element    //button[contains(text(),'Search Father')]    40    1
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=jwl_palastineid    40    1
    click on element    id=jwl_palastineid
    wait for element    id=jwl_palastineid_i
    send text to element    id=jwl_palastineid_i    ${GovtID}
    wait for element    id=WebResource_SearchIndividual
    Select IFrame    id=WebResource_SearchIndividual
    click on element    //button[contains(text(),'Search Father')]
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    ${father}    Get Text    //div[@id='header_process_jwl_individualcustomerid']//span[@class='ms-crm-Lookup-Item']
    ${father_new}    Get Text    //div[@id='jwl_individualcustomerid']//span[@class='ms-crm-Lookup-Item']
    Run Keyword If    '${father}' == '${father_new}'    Log    father added successfully
    ...    ELSE    Log    father not added successfully
    ${father_subs}    Get Text    //div[@id='header_process_jwl_subscriptionid']//span[@otypename='etel_subscription']
    ${father_offering}    Get Text    //div[@id='header_process_jwl_fatherofferingid']//span[@otypename='product']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Double Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Mother#
    wait for element    id=jwl_bi_familypackage|NoRelationship|Form|Mscrm.Form.jwl_bi_familypackage.Save    60    2
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=header_process_jwl_mothersubscriptionidid_cl    40    1
    Wait And Click Element    id=header_process_jwl_mothersubscriptionidid
    Wait And Click Element    id=header_process_jwl_mothersubscriptionidid_ledit
    Wait And Click Element    id=header_process_jwl_mothersubscriptionidid_i
    Wait And Click Element    //ul[@id='header_process_jwl_mothersubscriptionidid_i_IMenu']/li[1]
    sleep    2s
    Wait And Click Element    id=header_process_jwl_motheroffering
    Wait And Click Element    id=header_process_jwl_motheroffering_ledit
    Wait And Click Element    id=header_process_jwl_motheroffering_i
    Wait And Click Element    //ul[@id='header_process_jwl_motheroffering_i_IMenu']/li[1]
    sleep    2s
    Wait And Click Element    id=header_process_jwl_thresholdmother
    Wait And Click Element    //select[@id='header_process_jwl_thresholdmother_i']/option[3]
    ${mother_sub}    Get Text    //div[@id='header_process_jwl_mothersubscriptionidid']//span[@otypename='etel_subscription']
    ${mother_offering}    Get Text    //div[@id='header_process_jwl_motheroffering']//span[@otypename='product']
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Child#
    wait for element    id=jwl_bi_familypackage|NoRelationship|Form|Mscrm.Form.jwl_bi_familypackage.Save    60
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=header_process_jwl_childoonesubscriptionid    40    1
    sleep    10s
    Wait And Click Element    id=header_process_jwl_childoonesubscriptionid
    Wait And Click Element    id=header_process_jwl_childoonesubscriptionid_ledit
    Wait And Click Element    id=header_process_jwl_childoonesubscriptionid_i
    Wait And Click Element    //ul[@id='header_process_jwl_childoonesubscriptionid_i_IMenu']/li[1]
    sleep    2s
    Wait And Click Element    id=header_process_jwl_childoneofferingid
    Wait And Click Element    id=header_process_jwl_childoneofferingid_ledit
    Wait And Click Element    id=header_process_jwl_childoneofferingid_i
    Wait And Click Element    //ul[@id='header_process_jwl_childoneofferingid_i_IMenu']//span[contains(text(),'Child Package')]
    sleep    2s
    Wait And Click Element    id=header_process_jwl_thresholdchild1
    Wait And Click Element    //select[@id='header_process_jwl_thresholdchild1_i']/option[3]
    ${child_subs}    Get Text    //div[@id='header_process_jwl_childoonesubscriptionid']//span[@otypename='etel_subscription']
    ${child_offering}    Get Text    //div[@id='header_process_jwl_childoneofferingid']//span[@otypename='product']
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Summary#
    wait for element    id=jwl_bi_familypackage|NoRelationship|Form|Mscrm.Form.jwl_bi_familypackage.Save    60    2
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=jwl_subscriptionid_cl    40    1
    ${mother_subs_added}    Get Text    //div[@id='jwl_mothersubscriptionidid']//span[@otypename='etel_subscription']
    ${mother_offer_added}    Get Text    //div[@id='jwl_motheroffering']//span[@otypename='product']
    ${child_subs_added}    Get Text    //div[@id='jwl_childoonesubscriptionid']//span[@otypename='etel_subscription']
    ${child_offer_added}    Get Text    //div[@id='jwl_childoneofferingid']//span[@otypename='product']
    ${father_subs_added}    Get Text    //div[@id='jwl_subscriptionid']//span[@otypename='etel_subscription']
    ${father_offer_added}    Get Text    //div[@id='jwl_fatherofferingid']//span[@otypename='product']
    Run Keyword If    '${father_subs} ' == '${father_subs_added}'    Log    Father subscription added successfully
    ...    ELSE    Log    Father subscription not added successfully
    Run Keyword If    '${father_offering}' == '${father_offer_added}'    Log    Father Offering added successfully
    ...    ELSE    Log    Father Offering not added successfully
    Run Keyword If    '${mother_sub}' == '${mother_subs_added}'    Log    Mother subscription added successfully
    ...    ELSE    Log    Mother subscription not added successfully
    Run Keyword If    '${mother_offering}' == '${mother_offer_added}'    Log    Mother Offering added successfully
    ...    ELSE    Log    Mother Offering not added successfully
    Run Keyword If    '${child_subs}' \ == '${child_subs_added}'    Log    Child subscription added successfully
    ...    ELSE    Log    Child subscription not added successfully
    Run Keyword If    '${child_offering}' == '${child_offer_added}'    Log    Child Offering added successfully
    ...    ELSE    Log    Child Offering not added successfully
    Wait And Click Element    id=savefooter_statuscontrol
    wait for element    id=savefooter_statuscontrol
    Unselect Frame
    ##Verify Submit##
    Wait And Click Element    id=jwl_bi_familypackage|NoRelationship|Form|jwl.jwl_bi_familypackage.Submit.Button
    Sleep    10s
    ${Submitmessage}    Confirm Action
    Log    Submit message is-${Submitmessage}
    Sleep    5s
    wait for element    id=header_jwl_status
    ${status}    Get Text    //div[@id='header_jwl_status']/div/span
    Run Keyword If    '${status}' == 'Submitted'    VerifySubmit FamilyPackage    ${father_subs}    ${mother_sub}    ${child_subs}    ${mother_offer_added}
    ...    Log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}
    ...    ELSE    Log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}
    Run Keyword If    '${status}' == 'Closed'    VerifySubmit FamilyPackage    ${father_subs}    ${mother_sub}    ${child_subs}    ${mother_offer_added}
    ...    Log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}
    ...    ELSE    Log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}
    ##Verify Submit ended##

VerifySubmit FamilyPackage
    [Arguments]    ${father_subs}    ${mother_sub}    ${child_subs}
    Wait And Click Element    //div[@id='jwl_subscriptionid']//span[@otypename='etel_subscription']/span
    Unselect Frame
    wait for element    id=etel_subscription|NoRelationship|Form|Mscrm.Form.etel_subscription.Save    60    2
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Sleep    8s
    wait for element    id=FamilyMembers_header_h2    40    1
    click on element    id=FamilyMembers_header_h2
    wait for element    //div[@id='Family_divDataArea']
    wait for element    //tr[@otypename='jwl_family']/td[2]//span[@class='gridLui']//span[@class='ms-crm-LookupItem-Name']
    ${Father}    Get Text    //tr[@otypename='jwl_family']/td[2]//span[@class='gridLui']//span[@class='ms-crm-LookupItem-Name']
    ${Mother}    Get Text    //tr[@otypename='jwl_family']/td[3]//span[@class='gridLui']//span[@class='ms-crm-LookupItem-Name']
    ${Child}    Get Text    //tr[@otypename='jwl_family']/td[4]//span[@class='gridLui']//span[@class='ms-crm-LookupItem-Name']
    ${F}    Convert To Integer    ${Father}
    ${M}    Convert To Integer    ${Mother}
    ${C}    Convert To Integer    ${Child}
    ${Father_MSISDN}    Convert To Integer    ${father_subs}
    ${Mother_MSISDN}    Convert To Integer    ${mother_sub}
    ${Child_MSISDN}    Convert To Integer    ${child_subs}
    Run Keyword If    '${F}' == '${Father_MSISDN}' and '${M}' == '${Mother_MSISDN}' and '${C}' == '${Child_MSISDN}'    Log    Family Package Added successfully
    ...    ELSE    Log    Family Package not added Successfully

SelectTitleOfWindow
    [Arguments]    @{AllWindows}
    #@{AllWindows}    Create List    Order Capture: ORD0063356 - Microsoft Dynamics CRM    GRE
    ${Len}    Get Length    ${AllWindows}
    : FOR    ${I2}    IN RANGE    ${Len}
    \    ${R}    Run Keyword And Return Status    Should Not Contain    ${AllWindows[${I2}]}    Order Capture
    \    log    ${R}
    \    Run Keyword If    '${R}'=='True' and ${R}!='undefined'    Select Window    ${AllWindows[${I2}]}
    \    Run Keyword If    '${R}'=='True' and '${R}'=='undefined'    Select Window    ${AllWindows[${I2}]}
    \    Exit For Loop If    '${R}'=='True'
    sleep    5s

OfferChange
    [Arguments]    ${ChangeOffer}    ${PaymentType}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //table[@class='formSectionTable']//select[@id='paymentTypeSelect1']    60    2
    Wait And Click Element    //select[@id='paymentTypeSelect1']//option[contains(text(),'${PaymentType}')]
    wait for element    //div[@id='processStepsContainer']/div[3]//div[@class='processStepValue']    40    2
    wait for element    //div[@id='processStepsContainer']//div[@class='process_PmType']    20    2
    sleep    7s
    ${CO}    Get Text    //div[@id='processStepsContainer']//div[@class='process_PmType']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Log    ${CO}
    Should Be Equal    ${CO}    ${ChangeOffer}
    Wait And Click Element    //table[@class='formSectionTable']//select[@name='Reason']
    Wait And Click Element    //table[@class='formSectionTable']//select[@name='Reason']/option[2]
    Wait And Click Element    //div[@id='newCustomerSearchButtonGroup']//button[contains(text(),'Search')]
    wait for element    //table[@id='eligibleOffersDataGridBody']    60    2
    Wait And Click Element    //table[@id='eligibleOffersDataGridBody']/tbody/tr[1]//input[@type='checkbox']
    ${offer}    Get Text    //table[@id='eligibleOffersDataGridBody']/tbody/tr[1]//div[@class='dataGridCellContent']/span
    Log    ${offer}
    Wait And Click Element    //div[@id='offerChange-eligibleOffers']//span[contains(text(),'ADD TO BASKET')]
    wait for element    //div[@id='orderBasket']//table[@class='dataGridBody']    60    2
    ${offer_added}    Get Text    //div[@id='orderBasket']//table[@class='dataGridBody']/tbody/tr[1]//span[@class='lowerCellSection ng-binding']
    Log    ${offer_added}
    Should Be Equal    ${offer}    ${offer_added}
    ${package}    Get Text    //div[@id='processStepsContainer']/div[2]//div[@class='process_accountNumber']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Run Keyword if    '${offer}' == '${package}'    Log    Offer added successfully
    ...    ELSE    Log    Offer not added successfully
    ${MSISDN}    Get Text    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_AccountNo']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Log    ${MSISDN}
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Product Configuration#
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=productsConfigurationDataGridBody    60    1
    wait for element    id=orderBasket    40    1
    Sleep    10s
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Addon Expiration#
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=addonExpirationTitle    120    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Guarantee#
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=guarantee    60    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Payments
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=btnConfigure    60    1
    click on element    id=btnConfigure
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=jwl_collectionmethod    60    1
    Unselect Frame
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Sleep    5s
    Wait And Click Element    id=jwl_payment|NoRelationship|Form|Mscrm.Form.jwl_payment.SaveAndClose    60    1
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=etel_ordercapture|NoRelationship|Form|Mscrm.Form.etel_ordercapture.Save    40    1
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp    40    1
    select Iframe    id=WebResource_processapp
    wait for element    id=btnConfigure    60    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Summary#
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='offerSummaryContent']//span[@class='k-icon k-i-calendar']    60    2
    Sleep    10s
    ${A}    Get Current Date
    ${New_Date}    Convert Date    ${A}    result_format=%d/%m/%Y
    log    ${A}
    log many    ${New_Date}
    @{Day}    Split String    ${New_Date}    /
    log    ${Day}
    ${New_Date}    Get From List    ${Day}    0
    log    ${New_Date}
    Comment    ${DateForCode}    Evaluate    ${New_Date}
    click on element    //div[@id='offerSummaryContent']//span[@class='k-icon k-i-calendar']
    ${AllDates}    Get Matching Xpath Count    //div[@class='k-animation-container']//table//tbody//tr//td
    ${ForCount}    Evaluate    ${AllDates}+1
    : FOR    ${INDEX}    IN RANGE    1    7
    \    Comment    ${Datavaluerow}    Get Text    //div[@class='k-animation-container']//table//tbody//tr[${INDEX}]//td//a
    \    ${columnxpath}    Set Variable    //div[@class='k-animation-container']//table//tbody//tr[${INDEX}]
    \    SelectDateColumn    ${columnxpath}    ${New_Date}
    \    #Press Key    //div[@id='offerSummaryContent']//span[@class='k-icon k-i-calendar']    \\13
    Wait And Click Element    id=stageAdvanceActionContainer
    sleep    20s
    ${Submitmessage}    Confirm Action
    Log    Submit message is-${Submitmessage}
    Sleep    5s
    wait for element    id=header_jwl_status
    ${status}    Get Text    //div[@id='header_jwl_status']/div/span
    Run Keyword If    '${status}' == 'Submitted'    VerifyOfferChange    ${offer}    ${MSISDN}
    ...    ELSE    Log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}
    Run Keyword If    '${status}' == 'Closed'    VerifyOfferChange    ${offer}    ${MSISDN}
    ...    ELSE    Log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}

SelectDateColumn
    [Arguments]    ${columnxpath}    ${New_Date}
    : FOR    ${INDEX2}    IN RANGE    1    8
    \    ${xpathforolumn}    Set Variable    ${columnxpath}//td[${INDEX2}]//a
    \    log    ${xpathforolumn}
    \    ${column_value}    Get Text    ${xpathforolumn}
    \    Run Keyword If    '${column_value}'=='${New_Date}'    click on element    ${xpathforolumn}
    \    Exit For Loop If    '${column_value}'=='${New_Date}'
    sleep    7s

Bulk Data Creation
    [Arguments]    ${Bulkoperation}    ${Bulkdate}    ${Bulkname}    ${Bulktext}    ${FilePath}
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait And Click Element    //div[@id='jwl_operationtype']
    Select From List By Label    //select[@id='jwl_operationtype_i']    ${Bulkoperation}
    Wait And Click Element    //div[@id='jwl_scheduleddate']
    Input Text    //input[@id='DateInput']    ${Bulkdate}
    Unselect Frame
    Wait And Click Element    //li[@id='jwl_bulkoperation|NoRelationship|Form|Mscrm.Form.jwl_bulkoperation.Save']
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait And Click Element    //textarea[@id='createNote_notesTextBox']
    Input Text    //textarea[@id='createNote_notesTextBox']    ${Bulktext}
    Choose File    //button[@id='attachButton']    ${FilePath}/BulkOrder.xlsx
    Wait And Click Element    //div[@id='doneSpacer']
    ${BulkId}=    Get Text    //div[@id='FormTitle']
    Unselect Frame
    Wait And Click Element    //li[@id='jwl_bulkoperation|NoRelationship|Form|Mscrm.Form.jwl_bulkoperation.SaveAndClose']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    ${Bulk_data1}=    Get Text    //table[@id='gridBodyTable']/tbody/tr/td[2]/nobr
    ${Bulk_data2}=    Get Text    //table[@id='gridBodyTable']/tbody/tr/td[3]/nobr
    ${Bulk_datalist}=    Get Text    //table[@id='gridBodyTable']/tbody/tr
    Run Keyword If    '${Bulk_data1}'=='${BulkId}'    log to console    Bulk operation trigerred sucessfully
    Run Keyword If    '${Bulk_data2}'=='${Bulkname}'    log to console    Bulk operation trigerred sucessfully

Deactivate the customer
    [Arguments]    ${Deactivatereason}    ${DeactServiCe}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=subscriptions_header_h2
    click on element    id=subscriptions_header_h2
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${countofrows}    Get Matching Xpath Count    //div[@id='Subscription_divDataArea']//div[@id='gridBodyTable']/tbody/tr
    Log to Console    ${countofrows}
    ${countforloop}    Evaluate    ${countofrows}+1
    ${DeactivatedSubscriptionid}    Get Text    //table[@id='gridBodyTable']/tbody/tr/td[5]
    Wait And Click Element    //table[@id='gridBodyTable']/tbody/tr/td[2]/div
    : FOR    ${INDEX}    IN RANGE    2    ${countforloop}
    \    ${custtobedeactivated}    Get text    //table[@id='gridBodyTable']/tbody/tr[${index}]/td[4]/div
    \    Run keyword if    '${DeactServiCe}'=='${custtobedeactivated}'    Click Element    //table[@id='gridBodyTable']/tbody/tr[${index}]/td[2]/div
    \    Exit for loop    '${DeactServiCe}'=='${custtobedeactivated}'
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contact|NoRelationship|Form|jwl.contact.OrderCapture.Button
    click on element    id=contact|NoRelationship|Form|jwl.contact.OrderCapture.Button
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contact|NoRelationship|Form|jwl.contact.Button62.Button
    click on element    id=contact|NoRelationship|Form|jwl.contact.Button62.Button
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contact|NoRelationship|Form|jwl.contact.Button67.Button
    click on element    id=contact|NoRelationship|Form|jwl.contact.Button67.Button
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp    60    2
    select Iframe    id=WebResource_processapp
    wait for element    //textarea[@name='notesTextArea']    60    2
    send text to element    //textarea[@name='notesTextArea']    ${Deactivatereason}
    Wait And Click Element    id=stageAdvanceActionContainer
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    id=stageAdvanceActionContainer
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    id=stageAdvanceActionContainer
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    id=stageAdvanceActionContainer
    [Return]    ${DeactivatedSubscriptionid}

Verify Subscrition Deativated
    [Arguments]    ${Deactivatedcust}    ${DeactivatedSubscriptionid}
    Wait And Click Element    //id[@id='TabNode_tab0Tab-main']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=subscriptions_header_h2
    click on element    id=subscriptions_header_h2
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${Deactivated_sub}    get text    //table[@id='gridBodyTable']/tbody/tr[${index}]/td[2]/div
    Run Keyword If    '${Deactivated_sub}'=='No Subscriptions record found.'    log to console    Subscription deactivated sucessfully
    Else    Check if subscription is deactivated    ${DeactivatedSubscriptionid}

Check if subscription is deactivated
    [Arguments]    ${DeactivatedSubscriptionid}
    ${countofrows}    Get Matching Xpath Count    //div[@id='Subscription_divDataArea']//div[@id='gridBodyTable']/tbody/tr
    Log to Console    ${countofrows}
    ${countforloop}    Evaluate    ${countofrows}+1
    : FOR    ${INDEX}    IN RANGE    1    ${countforloop}
    \    ${Deactivated_sub}    Get text    //table[@id='gridBodyTable']/tbody/tr[${index}]/td[5]/div
    \    Run Keyword If    '${Deactivated_sub}'=='${DeactivatedSubscriptionid}'    log to console    Subscription not deactivated sucessfully
    \    Exit for loop    '${Deactivated_sub}'=='${DeactivatedSubscriptionid}'
    \    ...    ELSE    log    Subscription deactivated sucessfully

AddMCBService
    [Arguments]    ${MCBService}
    ##Modify Subscription##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='existingOfferingsGridBody']    40    1
    Wait And Click Element    //div[@id='existingproductsTitle']//span[contains(text(),'Add new Products')]
    wait for element    //div[@id='newProductsArea']//input[@name='addNewProductsFilterInput']    60    1
    send text to element    //div[@id='newProductsArea']//input[@name='addNewProductsFilterInput']    ${MCBService}
    Wait And Click Element    //table[@id='configDataGridBody']//span[contains(text(),'- Malicious Call Barring')]
    Wait And Click Element    //div[@id='newProductsArea']//span[contains(text(),'ADD TO BASKET')]
    ${MCBSelected}    Get Text    //table[@id='configDataGridBody']//span[contains(@title,'- Malicious Call Barring')]
    ${countofrows}    Get Matching Xpath Count    //div[@id='orderBasket']//div[@class='dataGridBodyScroll']//table//tbody//td[1]
    ${countforloop}    Evaluate    ${countofrows}+3
    : FOR    ${INDEX}    IN RANGE    2    ${countforloop}
    \    ${MCBfromorderbasket}    Get text    //div[@id='orderBasket']//div[@class='dataGridBodyScroll']//table//tbody//td[1]//div//span[${INDEX}]
    \    ${StatusInOrder}    Run keyword if    '${MCBSelected}'=='${MCBfromorderbasket}'    Get Text    //div[@id='orderBasket']//span[@title='Add-on']
    \    Run Keyword If    '${StatusInOrder}'=='(Add-on)'    log    Service is added succesfully
    \    ...    ELSE    log    Service is not added succesfully
    \    Exit For Loop If    '${MCBSelected}'=='${MCBfromorderbasket}'
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Configuration##
    Sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=fafServices    40    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Resources##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    wait for element    id=resourcesBlock    60    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Addon Expiration##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    wait for element    id=addonExpirationTitle    60    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Guarantees##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    wait for element    id=guarantee
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Payment##
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=divPayments    40    1
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame
    ##Summary##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp    40    1
    select Iframe    id=WebResource_processapp
    wait for element    //span[@class='k-select']    60    1
    sleep    8s
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp    40    1
    select Iframe    id=WebResource_processapp
    wait for element    id=stageAdvanceActionContainer
    Double Click Element    id=stageAdvanceActionContainer
    Sleep    40s
    ${Submitmessage}    Confirm Action
    Log    Submit message is-${Submitmessage}
    Sleep    120s
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    wait for element    header_jwl_status_d    40    1
    ${status}    Get Text    //div[@id='header_jwl_status_d']//div[@id='header_jwl_status']/div/span
    Run Keyword If    '${status}' == 'Submitted'    VerifyMCBServiceAdded
    ...    ELSE    Log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}
    Run Keyword If    '${status}' == 'Closed'    VerifyMCBServiceAdded
    ...    ELSE    Log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}

VerifyMCBServiceAdded
    wait for element    id=WebResource_processapp    60    2
    select Iframe    id=WebResource_processapp
    ${RatePlan}    Get Text    //div[@id='processStepsContainer']/div[2]//div[@id='processStep_corporateCustomer']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    wait for element    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_AccountNo']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Double Click Element    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_AccountNo']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    wait for element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    wait for element    id=contentIFrame0    40    1
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Sleep    10s
    wait for element    id=tab_7_header_h2    60    1
    Wait And Click Element    id=tab_7_header_h2
    Select IFrame    id=WebResource_productsview
    ${count}=    Set Variable    1
    : FOR    ${INDEX}    IN RANGE    1    20
    \    Log    ${count}
    \    ${a}    Get Text    //div[@id='planningPlanSection']//tr[${count}]/td[1]/div/span[2]
    \    Run Keyword If    '${a}' == '${RatePlan} - Other Services - Malicious Call Barring'    Log    MCB Service added successfully
    \    ...    ELSE    Log    MCB Service not added successfully
    \    Exit For Loop If    '${a}' == '${RatePlan} - Other Services - Malicious Call Barring'
    \    ${count}=    Evaluate    ${count}+4

GenerateCallDetail
    [Arguments]    ${Call_detail_date1}    ${Call_detail_date2}
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']
    click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.CallDetailStatement.Button']
    click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.CallDetailStatement.Button']
    Wait Until Keyword Succeeds    120s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    contentIFrame0
    Select IFrame    contentIFrame0
    Input Text    //input[@id='FromDate']    ${Call_detail_date1}
    Input Text    //input[@id='EndDate']    ${Call_detail_date2}
    Wait And Click Element    //button[@id='btnSaveDates']
    ${msisdn_call_detail}=    Get Text    //div[@id='FormTitle']
    Unselect Frame
    Wait And Click Element    //li[@id='jwl_bi_calldetailstatement|NoRelationship|Form|Mscrm.Form.jwl_bi_calldetailstatement.SaveAndClose']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    [Return]    ${msisdn_call_detail}

VerifyCallDetail
    [Arguments]    ${msisdn_call_detail}
    Open Browser    ${SIT_URL}    ie
    wait for element    contentIFrame0
    Select IFrame    contentIFrame0
    Wait And Click Element    //select[@id='slctPrimaryEntity']
    Click Element    //option[@value='jwl_bi_calldetailstatement']
    Unselect Frame
    Wait And Click Element    //a[@id='Mscrm.AdvancedFind.Groups.Show.Results-Large']
    Page Should Contain    Call Detail Statement Generation for ${msisdn_call_detail}
