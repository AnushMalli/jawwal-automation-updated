*** Settings ***
Resource          ../Utils/NavigateFunctions/Navigator.robot
Resource          ../Facilities/BaseCustomerFacilities.robot
Library           ExtendedSelenium2Library

*** Keywords ***
ClickResetCallBarringPassword
    Select IFrame    id=contentIFrame0
    Sleep    20s
    #Wait And Click Element    Xpath=.//*[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']/span/a/span
    #Wait And Click Element    Xpath=.//*[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']/span/a/span
    Comment    Select IFrame    id=contentIFrame1
    Wait And Click Element    Xpath=.//*[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.ResetCallBarring.Button']/span/a/span
    Double Click Element    Xpath=.//*[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.ResetCallBarring.Button']/span/a/span
    Sleep    10s
    Confirm Action
    Sleep    10s
    Confirm Action
    Unselect Frame
