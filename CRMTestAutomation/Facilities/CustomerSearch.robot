*** Settings ***
Resource          ../Utils/NavigateFunctions/Navigator.robot
Resource          ../Facilities/BaseCustomerFacilities.robot
Library           ExtendedSelenium2Library
Resource          ../Resources/MainFunctions.robot

*** Keywords ***
ClickLookForSelectField
    Select IFrame    id=contentIFrame0
    Wait And Click Element    xpath=//div[@id='SimpleSearch']/table/tbody/tr/td[2]
    Unselect Frame

CheckSelectFieldOptions
    Select IFrame    id=contentIFrame0
    Wait And Click Element    xpath=//div[@id='SimpleSearch']/table/tbody/tr/td[2]/select/option[4]
    Unselect Frame

SendSearchValueAndSearch
    [Arguments]    ${value}    # searchName
    Select IFrame    id=contentIFrame0
    Wait And Click Element    id=SearchText
    send text to element    id=SearchText    ${value}
    Wait And Click Element    xpath=//a[@onclick=' doSimpleSearch() ']
    Unselect Frame

VerifyCustomerSerachPage
    Select IFrame    id=contentIFrame0
    ${present}=    Run Keyword And Return Status    Element Should Be Visible    css=span.tCustomerSearch
    Run Keyword If    ${present}    Add Result    PASS    UC01    Verify that customer search page is displayed     Customer Search page is visible
    ...    Customer search page verificatiom completed
    Unselect Frame

ClickOnLookForSelectFieldOptions
    [Arguments]    ${LookFor}
    Select IFrame    id=contentIFrame0
    Wait And Click Element    xpath=//select[@id='LookFor']//option[contains(text(),'${LookFor}')]
    Unselect Frame

VerifySearchedElementIfMultiplePresent
    [Arguments]    ${Name}
    Select IFrame    id=contentIFrame0
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[2]/table/tbody/tr/td[4]/div
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to evaluate the correct movie name ###
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${NamefromWeb}=    Get Text    xpath=//div[2]/table/tbody/tr[${INDEX}]/td[4]/div
    \    log    ${NamefromWeb}
    \    ${a}    Split String    ${NamefromWeb}
    \    Run Keyword If    '${a[0]}'=='${Name}'    Double Click Element    xpath=//div[2]/table/tbody/tr[${INDEX}]/td[4]/div
    \    Exit for loop
    Unselect Frame
    Select IFrame    id=contentIFrame0
    wait for element    xpath=//img[@alt='FormSections_NavigationFlyOut_Button']
    ${present}=    Run Keyword And Return Status    Element Should Be Visible    xpath=//h1
    Run Keyword If    ${present}    Log To Console    Searched Element is visible in page
    Unselect Frame

VerifySearchElement
    Select IFrame    id=contentIFrame0
    wait for element    xpath=//img[@alt='FormSections_NavigationFlyOut_Button']    1m
    ${present}=    Run Keyword And Return Status    Element Should Be Visible    xpath=//h1
    Run Keyword If    ${present}    Log To Console    Searched Element is visible in page
    Unselect Frame
