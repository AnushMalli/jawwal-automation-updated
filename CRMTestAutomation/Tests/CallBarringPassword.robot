*** Settings ***
Suite Teardown    Close All Browsers
Test Setup        Go to CRM
Test Teardown     When Test Failed Take Screenhot
Resource          ../Resources/MainFunctions.robot
Resource          ../Resources/Variables.txt
Library           ExtendedSelenium2Library
Library           Collections
Library           RequestsLibrary
Library           Process
Resource          ../Facilities/BaseCustomerFacilities.robot
Resource          ../Facilities/CustomerFacilities.robot
Resource          ../Utils/NavigateFunctions/Navigator.robot
Resource          ../Facilities/CallBarringPassword.robot

*** Test Cases ***
569-ResetCallBarringPasswordFromSubscriptionPanel
    Run Keyword And Continue On Failure    pendingemailwarning
    Run Keyword And Continue On Failure    closeexploreCMR
    Go To Spesific Individual Customer    CUST0000000309
    OpenSelectedSubscriptionBasedOnContractID
    ClickResetCallBarringPassword
    Close Browser

513-ResetCallBarringPasswordButton
    Go To Spesific Individual Customer    Vineet6
    OpenAccountSummaryPage
    GoToSubsDetailForSelectedIndv
    Close Browser

101-ResetCallBarringBugFixCheck
    Go to Spesific Corporate Customer    CORP000097
    OpenAccountSummaryPage
    GoToSubsDetailForSelectedCorp
    CallBarringPassword.ClickResetCallBarringPassword
    Sleep    5s
    Confirm Action
    Comment    Sleep    10s
    Comment    Should Be Equal    Error Happened: JBoss Error Message: Server Interval Error    Confirm Action
    Close Browser
